@extends('layouts.app')

@if($group)
@section('button', "返回")
@endif

@section('title', $group ?: "大厅")

@section('css')
<style rel="stylesheet">
.top-room-container {
	position: absolute;
	top: 40px;
	left: 65px;
	color: black;
	font-style: italic;
	text-shadow: 1px 1px 2px grey, 2px 2px 3px grey;
	font-size: 20px;
}
.top-room-text {
	color: black;
	text-shadow: 1px 1px 2px grey, 2px 2px 3px grey;
	margin-left: 2px;
	margin-right: 2px;
}
.bottom-room-container {
	position: absolute;
	top: 50px;
	right: 75px;
	color: black;
	font-style: italic;
	text-shadow: 1px 1px 2px grey, 2px 2px 3px grey;
}
.bottom-room-text {
	color: black;
	text-shadow: 1px 1px 2px grey, 2px 2px 3px grey;
	margin-left: 2px;
	margin-right: 2px;
}
.room-title {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

.standalone {
	position: absolute;
	left: 15px;
	top: 15px;
	padding: 5px;
	border-radius: 5px;
	color: white;
	background: red;
	transform: translateX(50%);
}	

.lobby-name{
    margin: 3px 0px 0px;
    font-weight: 550;
    letter-spacing: 0.00938em;
    color: rgb(255, 255, 255);
    text-align: center;
    font-size: 16px;
    line-height: 14px;
}
		
.return-button {
	position: absolute;
	top: 5px;
	left: 5px;
	background: brown;
	z-index: 100;
}
</style>
@endsection

@section('theme', "url(" . asset('images/bg-anim.gif') . "); background-color: #303030")

@section('content')
<button class="return-button" onclick="handleBack()"><i class="feather icon-x"></i></button>

<div class="container-fluid" style="background: linear-gradient(rgb(36, 45, 140) 0%, rgb(7, 11, 50) 58.12%, rgb(23, 31, 60) 100%);">
    <div class="row justify-content-center">
        <div class="col-md-12" style="margin-top: 50px;">
			@if (session('status'))
				<div class="alert alert-success" role="alert">
					{{ session('status') }}
				</div>
			@endif
			@if ($errors->has('message'))
				<div class="alert alert-warning" role="alert">
					{{ $errors->get('message')[0] }}
				</div>
			@endif
			
			@if (($tBannerGroup = $banner_groups->where("key", "lobby_top")->first()) && $tBannerGroup->banners->count() > 0)
				<div id="silder" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
				@foreach($tBannerGroup->banners as $index => $tBanner)
					<li data-target="#silder" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}"></li>
				@endforeach
				</ol>
				<div class="carousel-inner">
				@foreach($tBannerGroup->banners as $index => $tBanner)
					<div class="carousel-item{{ $index == 0 ? ' active' : '' }}">
					  <img class="d-block w-100" src="{{ asset('storage/' . $tBanner->url) }}" onerror="setDefaultImg(this)" alt="{{ $tBanner->caption }}" />
					  @if($tBanner->show_caption)
					  <div class="carousel-caption d-none d-md-block">
						<h5>{{ $tBanner->caption }}</h5>
						<p>{{ $tBanner->description }}</p>
					  </div>
					  @endif
					</div>
				@endforeach
				</div>
				  <a class="carousel-control-prev" href="#silder" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#silder" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
			@endif
			
			<div class="row" style="margin: 10px 0;">
				@if($groups)
					@foreach($groups as $room)
					<div class="col-sm-2">
						<a href="{{ route('lobby', ['group' => $room->group]) }}" class="lobby-box">
							<div class="row form-group">
								<img class="col-12" style="border-radius: 30px;" src="{{ asset('images/default.jpg') }}" />							
								<div class="top-room-container">
									~<b class="top-room-text">{{ number_format($room->rate, 2) }}</b>倍
								</div>
								<h1 class="room-title">{{ $room->name }}</h1>
								<div class="bottom-room-container">
									已有<b class="bottom-room-text">{{ $room->total }}</b>人参与
								</div>
							</div>
							<p class="lobby-name">{{ $room->name }}</p>
						</a>
					</div>
					@endforeach
				@else
					@foreach($rooms as $room)
					<div class="col-sm-2">
						<a href="{{ route('room', ['id' => $room->id]) }}" class="lobby-box">
							<div class="row form-group">
								<img class="col-12" src="{{ $room->image_url }}" />
								@if($room->is_standalone)
								<div class="standalone">个人</div>
								@endif
								<img>
							</div>
							<p class="lobby-name">{{ $room->name }}</p>
						</a>
					</div>
					@endforeach
				@endif
			</div>
        </div>
    </div>
</div>
@endsection

@section("js")
<script>
	function setDefaultImg(elem) {
		elem.src = "{{ asset('images/banner.svg') }}";
	}
</script>
@endsection