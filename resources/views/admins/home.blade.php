@extends('admins.layouts.app')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">看板</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">看板</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>用户数量</small>
                <h5 class="text-info">{{ $user_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
	@if ($errors->has('message'))
		<div class="row">
			<div class="col-12">
				<div class="alert alert-warning" role="alert">
					{{ $errors->get('message')[0] }}
				</div>
			</div>
		</div>
	@endif
	@if($user->role)
		<div class="row">
			<!-- column -->
			<div class="col-sm-12 col-lg-4">
				<div class="card card-hover">
					<div class="card-body">
						<div class="d-flex">
							<div class="mr-4">
								<small>上月总盈利</small>
								<h4 class="mb-0">${{ number_format($prev_total_earn, 2) }}</h4>
							</div>
							<div class="chart ml-auto">
								<a>查看详细</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- column -->
			<div class="col-sm-12 col-lg-4">
				<div class="card card-hover bg-green">
					<div class="card-body">
						<div class="d-flex">
							<div class="mr-4">
								<small>本月总充值</small>
								<h4 class="mb-0">${{ number_format($monthly_total_topup, 2) }}</h4>
							</div>
							<div class="chart ml-auto">
								<a>查看详细</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- column -->
			<div class="col-sm-12 col-lg-4">
				<div class="card card-hover bg-red">
					<div class="card-body">
						<div class="d-flex">
							<div class="mr-4">
								<small>本月总提取</small>
								<h4 class="mb-0">${{ number_format($monthly_total_withdraw, 2) }}</h4>
							</div>
							<div class="chart ml-auto">
								<a>查看详细</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@if(!$user->role->regional)
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-body">
							<!-- title -->
							<div class="d-md-flex align-items-center">
								<div>
									<h4 class="card-title">本月最多下注的房间</h4>
									<p class="card-subtitle"></p>
								</div>
								<div class="ml-auto">
									<div class="dl">
										<!--<select class="custom-select">
											<option value="0" selected="">月</option>
											<option value="1">日</option>
											<option value="2">星期</option>
											<option value="3">年</option>
										</select>-->
									</div>
								</div>
							</div>
							<!-- title -->
						</div>
						<div class="table-responsive">
							<table class="table v-middle">
								<thead>
									<tr class="bg-light">
										<th class="border-top-0">房间名</th>
										<th class="border-top-0">下注次数</th>
										<th class="border-top-0">下注总金额</th>
										<th class="border-top-0">当前彩票期</th>
									</tr>
								</thead>
								<tbody>
									@foreach($rooms as $room)
									<tr>
										<td>{{ $room->name }}</td>
										<td>{{ $room->bettingRecords()->count() }}</td>
										<td>{{ $room->bettingRecords()->sum('amount') }}</td>
										<td>{{ $room->current ? ($room->current->version . "期") : "-" }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="row">
			<!-- column -->
			<div class="col-lg-6">
				<div class="card card-hover">
					<div class="card-body">
						<h4 class="card-title">最新下注</h4>
						<table class="table table-striped table-hover table-borderless table-vcenter font-size-sm">
							<thead>
								<tr class="text-uppercase">
									<th class="font-w700">房间</th>
									<th class="font-w700">用户</th>
									<th class="d-none d-sm-table-cell font-w700">时间</th>
									<th class="font-w700">结算</th>
									<th class="d-none d-sm-table-cell font-w700 text-right" style="width: 120px;">金额</th>
								</tr>
							</thead>
							<tbody>
								@foreach($betting_records as $item)
								<tr>
									<td>
										<span class="font-w600">{{ $item->betting && $item->betting->room ? $item->betting->room->name : "" }}</span>
									</td>
									<td>
										<span class="font-w600">{{ $item->user ? $item->user->name : "" }}</span>
									</td>
									<td class="d-none d-sm-table-cell">
										<span class="font-w600">{{ $item->created_at->diffForHumans() }}</span>
									</td>
									<td>
										<span class="font-w600">{{ $item->won != null ? ("$" . number_format($item->won, 2)) : "未开奖" }}</span>
									</td>
									<td class="d-none d-sm-table-cell text-right">
										${{ number_format($item->amount, 2) }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- column -->
			<div class="col-lg-6">
				<div class="card has-shadow">
					<div class="card-body border-top">
						<h4 class="card-title">本月前10充值用户</h4>
						<table class="table mb-0">
						<thead>
							<tr class="text-uppercase">
								<th class="font-w700">用户</th>
								<th class="font-w700">已充值</th>
								<th class="font-w700">当前余额</th>
								<th class="font-w700">盈利</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $item)
							@php
								$monthlyBettings = $item->bettingRecords()
								->where('won', '!=', null)
								->whereYear('betting_records.created_at', $today->year)
								->whereMonth('betting_records.created_at', $today->month)
								->get();
								$earn = $monthlyBettings->sum('won') - $monthlyBettings->sum('amount');
							@endphp
							<tr>
								<td class="font-w600">{{ $item->name }}</td>
								<td class="font-w600">${{ number_format($item->total_topup, 2) }}</td>
								<td class="font-w600">${{ number_format($item->totalBalance, 2) }}</td>
								<td class="font-w600 {{ $earn != 0 ? ($earn > 0 ? 'color-green' : 'color-red') : '' }}">
									{{ ($earn != 0 ? ($earn > 0 ? '+' : '-') : '') . "$" . number_format($earn, 2) }}
								</td>
							</tr>
							@endforeach
						</tbody>
					  </table>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
