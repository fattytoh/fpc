@php
	$user = Auth::guard('admin')->user();
	$system = [];
	$systemMenus = [
		['named' => 'admin.banner', 'icon' => "icon-film", 'title' => __('广告')],
		['named' => 'admin.staff', 'icon' => "icon-users", 'title' => __('职员')],
		['named' => 'admin.room', 'icon' => "icon-home", 'title' => __('房间')],
		['named' => 'admin.betting', 'icon' => "icon-gift", 'title' => __('彩票')],
	];
	foreach($systemMenus as $menu) {
		if($menu['route'] = RouteGuard::check($menu['named'])) {
			array_push($system, $menu);
		}
	}
	$users = [];
	$userMenus = [
		['named' => 'admin.user', 'icon' => "icon-users", 'title' => __('用户')],
		['named' => 'admin.topup.record', 'icon' => "icon-log-in", 'title' => __('充值记录')],
		['named' => 'admin.withdraw.record', 'icon' => "icon-log-out", 'title' => __('提取记录')],
		['named' => 'admin.betting.record', 'icon' => "icon-award", 'title' => __('下注记录')],
		['named' => 'admin.betting.record.4d', 'icon' => "icon-award", 'title' => __('4D下注记录')],
		['named' => 'admin.invitation.record', 'icon' => "icon-link", 'title' => __('邀请记录')]
	];
	foreach($userMenus as $menu) {
		if($menu['route'] = RouteGuard::check($menu['named'])) {
			array_push($users, $menu);
		}
	}
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}后台</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- DataTable -->
	<link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/editor.dataTables.min.css') }}" rel="stylesheet">
	<!-- Bootadmin -->
    <link href="{{ asset('css/bootadmin/library.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootadmin/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jqueryui-flat/jquery-ui.min.css') }}" rel="stylesheet">
	<style>
	.avatar > img {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	</style>
    @yield('css')
	
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>
	<script>
		const pusherKey = "{{ config('broadcasting.connections.pusher.key') }}";
		const host = "{{ url('/admin') }}";
	</script>
    <!-- SweetAlert2 -->
	<script src="{{ asset('js/sweetalert2@9.js') }}" defer></script>
	<!-- DataTable -->
	<script type="text/javascript" src="{{ asset('js/datatables.min.js') }}" defer></script>
	<script type="text/javascript" src="{{ asset('js/dataTables.editor.min.js') }}" defer></script>
	<!-- Moment -->
	<script type="text/javascript" src="{{ asset('js/moment.min.js') }}" defer></script>
	<!-- Bootadmin -->
    <script type="text/javascript" src="{{ asset('js/bootadmin/library.min.js') }}" defer></script>
	<script type="text/javascript" src="{{ asset('js/bootadmin/main.js') }}" defer></script>
	@yield('js')
</head>
<body id="landing" class="	sidebar-open">
	<div class="loading">
		<div class="loading-center"><img src="{{ asset('images/loading/running.gif') }}" alt="Loading" /></div>
	</div>
	<div class="page-container animsition">
		<div id="app">
			<div class="topbar">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-5 hidden-xs">
							<div class="logo">
								<a href="{{ route('admin.dashboard') }}">
									<span class="logo-full">{{ config('app.name', 'Laravel') }}后台</span>
								</a>
							</div>
							<a href="#" class="menu-toggle wave-effect">
								<i class="feather icon-menu"></i>
							</a>
						</div>
						<div class="col-md-7 text-right">
							<ul>
								<!-- Profile Menu -->
								<li class="btn-group user-account">
									<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<div class="user-content">
											<div class="user-name">{{ $user->name }}</div>
											<div class="user-plan">{{ $user->role ? $user->role->name : "无职位" }}</div>
										</div>
										<div class="avatar">
											<img src="{{ asset('images/user.png') }}" alt="profile" />
										</div>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a class="animsition-link dropdown-item wave-effect" href="{{ route('admin.logout') }}"
											   onclick="event.preventDefault();
															 document.getElementById('logout-form').submit();">
												<i class="feather icon-log-in"></i> {{ __('登出') }}
											</a>

											<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
												@csrf
											</form>
										</li>
									</ul>
								</li>
								<!-- Notification Menu -->
								<li class="btn-group notification">
									<a href="javascript:;" class="btn dropdown-toggle wave-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="feather icon-bell"><span class="notification-count" style="display: none;">-</span></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
									</ul>
								</li>
								<li class="mobile-menu-toggle">
									<a href="#" class="menu-toggle wave-effect">
										<i class="feather icon-menu"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div class="sidebar">
				<div class="logo">
					<a href="{{ route('admin.dashboard') }}">
						<span class="logo-emblem">目录</span>
						<span class="logo-full">{{ config('app.name', 'Laravel') }}后台</span>
					</a>
				</div>
				<ul id="sidebarCookie">
					<li class="spacer"></li>
					<li class="profile">
						<span class="profile-image">
							<img src="{{ asset('images/user.png') }}" alt="profile" />
						</span>
						<span class="profile-name">
							{{ Auth::guard('admin')->user()->name }}
						</span>
						<span class="profile-info">
							{{ $user->role ? $user->role->name : "无职位" }}
						</span>
					</li>
					<li class="spacer"></li>
					<li class="title">
						<i class="feather icon-more-horizontal"></i>
						<span class="menu-title">主要</span>
					</li>
					<li class="nav-item">
						<a href="{{ route('admin.dashboard') }}" class="nav-link wave-effect nav-single">
							<i class="feather icon-bar-chart-2"></i>
							<span class="menu-title">{{ __('看板') }}</span>
						</a>
					</li>
					@if(count($system) > 0)
					<li class="nav-item">
						<a class="nav-link wave-effect collapsed wave-effect" data-parent="#sidebarCookie" data-toggle="collapse" href="#navSystem" aria-expanded="false" aria-controls="page-dashboards">
							<i class="feather icon-globe"></i>
							<span class="menu-title">系统</span>
							<i class="feather icon-chevron-down down-arrow"></i>
						</a>
						<div class="collapse" id="navSystem">
							<ul class="flex-column sub-menu">
								@foreach($system as $item)
								<li class="nav-item">
									<a href="{{ $item['route'] }}" class="nav-link wave-effect">
										<i class="feather {{ $item['icon'] }}"></i>
										<span class="menu-title">{{ $item['title'] }}</span>
									</a>
								</li>
								@endforeach
							</ul>
						</div>
					</li>
					@endif
					@if(count($users) > 0)
					<li class="nav-item">
						<a class="nav-link wave-effect collapsed wave-effect" data-parent="#sidebarCookie" data-toggle="collapse" href="#navManagement" aria-expanded="false" aria-controls="page-dashboards">
							<i class="feather icon-grid"></i>
							<span class="menu-title">管理</span>
							<i class="feather icon-chevron-down down-arrow"></i>
						</a>
						<div class="collapse" id="navManagement">
							<ul class="flex-column sub-menu">
								@foreach($users as $item)
								<li class="nav-item">
									<a href="{{ $item['route'] }}" class="nav-link wave-effect">
										<i class="feather {{ $item['icon'] }}"></i>
										<span class="menu-title">{{ $item['title'] }}</span>
									</a>
								</li>
								@endforeach
							</ul>
						</div>
					</li>
					@endif
					@if(RouteGuard::check('admin.setting'))
					<li class="title">
						<i class="feather icon-more-horizontal"></i>
						<span class="menu-title">其他</span>
					</li>
					<li class="nav-item">
						<a href="{{ route('admin.setting') }}" class="nav-link wave-effect nav-single">
							<i class="feather icon-settings"></i>
							<span class="menu-title">{{ __('设定') }}</span>
						</a>
					</li>
					@endif
				</ul>
			</div>

			<main>
				@yield('content')
			</main>
		</div>
	</div>
</body>
</html>
