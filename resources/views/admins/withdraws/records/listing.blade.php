@extends('admins.layouts.app')

@section('css')
    <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">提取记录</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">提取记录</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>记录数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		let interval;
		const dtOpts = {
			listingUrl: "{{ Route('admin.withdraw.record') }}",
			updateUrl: "{{ Route('admin.withdraw.record.update') }}",
			deleteUrl: "{{ Route('admin.withdraw.record.delete') }}",
			order: [[ 3, "desc" ]],
			fields: [{
				label: "{{ __('用户识别序号') }}",
				name: "user_id",
				type: "readonly"
			}, {
				label: "{{ __('用户名称') }}",
				name: "user.name",
				type: "readonly"
			}, {
				label: "{{ __('金额') }}",
				name: "amount",
			}, {
				label: "{{ __('状态') }}",
				name: "status",
				fieldInfo: "提取完成状态会扣除用户金额，删除或更改提取完成记录将会还原提取金额。",
				type: "select",
				options: [{
					label: "申请中", value: "申请中",
				}, {
					label: "提取处理中", value: "提取处理中",
				}, {
					label: "提取完成", value: "提取完成",
				}, {
					label: "取消提取处理中", value: "取消提取处理中",
				}, {
					label: "已取消提取", value: "已取消提取",
				}]
			}, {
				label: "{{ __('备注') }}",
				name: "remrk",
				type: "textarea"
			}],
			columns: [{
				title: "{{ __('用户') }}",
				data: "user.name",
				defaultContent: "已删除"
			}, {
				title: "{{ __('金额') }}",
				data: "amount"
			}, {
				title: "{{ __('状态') }}",
				data: "status",
				render: function(data, dt, row) {
					return row.deleted_at ? "已取消提取" : (data ? data : "申请中");
				}
			}, {
				title: "{{ __('批准提取的管理员') }}",
				data: "approved",
				render: function(data, dt, row) {
					return data ? data.name : (row.approved_id ? ("管理员账号已删除，识别号为: " + row.approved_id) : "未曾批准");
				}
			}, {
				title: "{{ __('批准取消的管理员') }}",
				data: "rejected",
				render: function(data, dt, row) {
					return data ? data.name : (row.rejected_id ? ("管理员账号已删除，识别号为: " + row.rejected_id) : "未曾批准");
				}
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更改') }}",
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除') }}",
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}, {
					text: "{{ __('发送Whatsapp') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						window.open("https://web.whatsapp.com/send?text=用户"
							+ data.user?.name + data.status + "，金额" + data.amount + "元。");
					}
				}, {
					text: "{{ __('发送Telegram') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						window.open("https://t.me/share/url?url={{ url('/') }}&text=用户"
							+ data.user?.name + data.status + "，金额" + data.amount + "元。");
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection