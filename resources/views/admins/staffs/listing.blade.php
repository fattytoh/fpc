@extends('admins.layouts.app')

@section('header')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">职员</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">职员</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>职员数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const dtOpts = {
			listingUrl: "{{ Route('admin.staff') }}",
			createUrl: "{{ RouteGuard::check('admin.staff.create') }}",
			updateUrl: "{{ RouteGuard::check('admin.staff.update') }}",
			deleteUrl: "{{ RouteGuard::check('admin.staff.delete') }}",
			order: [[ 0, "asc" ]],
			fields: [{
				label: "{{ __('职位') }}",
				name: "role_id",
				type: "select",
				options : [{ id: "", name: "{{ __('无') }}" }].concat({!! $roles !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('职员名称') }}",
				name: "name",
			}, {
				label: "{{ __('账号') }}",
				name: "username",
			}, {
				label: "{{ __('密码') }}",
				name: "password",
				type: "password",
				attr: {
					autocomplete: 'off'
				}
			}, {
				label: "{{ __('确认密码') }}",
				name: "password_confirmation",
				type: "password",
				attr: {
					autocomplete: 'off'
				}
			}],
			columns: [{
				title: "{{ __('职员名称') }}",
				data: "name",
				responsivePriority: 1,
			}, {
				title: "{{ __('账号') }}",
				data: "username",
				responsivePriority: 2,
			}, {
				title: "{{ __('职位') }}",
				data: "role.name",
				defaultContent: "无",
				responsivePriority: 4,
			}, {
				title: "{{ __('最后登录IP') }}",
				data: "last_login_ip",
				responsivePriority: 3,
				defaultContent: "{{ _('无登录记录') }}"
			}, {
				title: "{{ __('加入日期') }}",
				data: "created_at",
				responsivePriority: 5,
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
				@if(RouteGuard::check('admin.staff.create'))
					extend: "create",
					editor: editor,
					text: "{{ __('增加') }}"
				}, {
				@endif
				@if(RouteGuard::check('admin.staff.update'))
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更新') }}"
				}, {
				@endif
				@if(RouteGuard::check('admin.staff.delete'))
					extend: "remove",
					editor: editor,
					text: "{{ __('删除') }}"
				}, {
				@endif
				@if(RouteGuard::check('admin.role'))
					text: "{{ __('职位管理') }}",
					action: function (e, dt, node, config) {
						window.open("{{ route('admin.role') }}");
					}
				}, {
					text: "{{ __('生成API密匙') }}",
					extend: "selected",
					action: function (e, dt, node, config) {
						const ids = dt.rows({ selected: true }).data().toArray().map(x => x.id);
						if(ids.length > 0) {
							$.ajax({
								url: "{{ route('admin.staff.api') }}",
								data: {
									ids: ids,
								},
								headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								},
								type: "POST"
							}).done(function(res) {
								let html = "";
								res.users.forEach(user => {
									html += user.name + ": " + user.api_token + "\n";
								});
								Swal.fire({
									title: "密匙生成",
									html: html,
									icon: "success"
								});
							}).fail(function(err) {
								Swal.fire("发生错误", err.responseJSON.message, "error");
							});
						}
					}
				}, {
				@endif
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection