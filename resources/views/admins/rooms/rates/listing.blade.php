@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">{{ $room->name }}赔率</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.room') }}">房间</a></li>
                    <li class="breadcrumb-item active" aria-current="page">赔率</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>-</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		let interval;
		
		const dtOpts = {
			listingUrl: "{{ Route('admin.room.rate', ['id' => $room->id]) }}",
			createUrl: "{{ Route('admin.room.rate.create', ['id' => $room->id]) }}",
			updateUrl: "{{ Route('admin.room.rate.update', ['id' => $room->id]) }}",
			deleteUrl: "{{ Route('admin.room.rate.delete', ['id' => $room->id]) }}",
			order: [[ 0, "asc" ]],
			fields: [{
				label: "{{ __('名字') }}",
				name: "name",
				type: "text",
			}, {
				label: "{{ __('赔率') }}",
				name: "rate",
				type: "text",
			}],
			columns: [{
				title: "{{ __('玩法') }}",
				data: "group.name",
				defaultContent: "",
				responsivePriority: 1,
			}, {
				title: "{{ __('名字') }}",
				data: "name",
				responsivePriority: 1,
			}, {
				title: "{{ __('赔率') }}",
				data: "rate",
				responsivePriority: 5
			}],
			buttons: function(editor) {
				return [{
					extend: "edit",
					editor: editor,
					text: "{{ __('调整赔率') }}"
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload(null, false);
					}
				}];
			}
		}
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection