@extends('admins.layouts.app')

@section('css')
    <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">邀请记录</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">邀请记录</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>记录数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		let interval;
		const dtOpts = {
			listingUrl: "{{ Route('admin.invitation.record') }}",
			order: [[ 4, "desc" ]],
			columns: [{
				title: "{{ __('用户') }}",
				data: "inviter.name",
				defaultContent: "已删除"
			}, {
				title: "{{ __('邀请人') }}",
				data: "invited",
				render: function(data, dt, row) {
					return data ?
						("<b style='color: green;' title='" + data.created_at + "'>"
						+ data.name + "</b>") :
						("<i style='color: orange;'>" + row.mobile + "</i>");
				}
			}, {
				title: "{{ __('渠道') }}",
				data: "from",
				defaultContent: "系统"
			}, {
				title: "{{ __('结果') }}",
				data: "status",
				defaultContent: "等待中"
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					text: "{{ __('Refresh') }}",
					action: function (e, dt) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection