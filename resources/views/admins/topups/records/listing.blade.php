@extends('admins.layouts.app')

@section('css')
    <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">充值记录</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">充值记录</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>记录数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		let interval;
		const dtOpts = {
			listingUrl: "{{ Route('admin.topup.record') }}",
			createUrl: "{{ Route('admin.topup.create') }}",
			updateUrl: "{{ Route('admin.topup.update') }}",
			deleteUrl: "{{ Route('admin.topup.delete') }}",
			order: [[ 4, "desc" ]],
			fields: [{
				label: "{{ __('用户') }}",
				name: "user_id",
				type: "select",
				options : [{ id: "", name: "{{ __('选择用户') }}" }].concat({!! $users !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('金额') }}",
				name: "amount",
			}, {
				label: "{{ __('渠道') }}",
				name: "method_id",
				type: "select",
				options : [{ id: "", name: "{{ __('选择渠道') }}" }].concat({!! $methods !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('状态') }}",
				name: "status",
				fieldInfo: "充值完成状态会增加用户金额，删除或更改充值完成记录将会扣除充值金额。",
				type: "select",
				options: [{
					label: "等待处理中", value: "等待处理中",
				}, {
					label: "充值处理中", value: "充值处理中",
				}, {
					label: "充值完成", value: "充值完成",
				}, {
					label: "退款处理中", value: "退款处理中",
				}, {
					label: "已退款", value: "已退款",
				}]
			}, {
				label: "{{ __('备注') }}",
				name: "remrk",
				type: "textarea"
			}],
			columns: [{
				title: "{{ __('用户') }}",
				data: "user.name",
				defaultContent: "已删除"
			}, {
				title: "{{ __('金额') }}",
				data: "amount"
			}, {
				title: "{{ __('渠道') }}",
				data: "method.name",
				defaultContent: "系统"
			}, {
				title: "{{ __('状态') }}",
				data: "status",
				render: function(data, dt, row) {
					return row.deleted_at ? "已退款" : (data ? data : "等待处理中");
				}
			}, {
				title: "{{ __('批准提取的管理员') }}",
				data: "approved",
				render: function(data, dt, row) {
					return data ? data.name : (row.approved_id ? ("管理员账号已删除，识别号为: " + row.approved_id) : "未曾批准");
				}
			}, {
				title: "{{ __('批准取消的管理员') }}",
				data: "rejected",
				render: function(data, dt, row) {
					return data ? data.name : (row.rejected_id ? ("管理员账号已删除，识别号为: " + row.rejected_id) : "未曾批准");
				}
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "create",
					editor: editor,
					text: "{{ __('充值') }}"
				}, {
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更改') }}"
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除') }}"
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt) {
						dt.ajax.reload();
					}
				}, {
					text: "{{ __('发送Whatsapp') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						window.open("https://web.whatsapp.com/send?text=用户"
							+ data.user?.name + data.status + "，金额" + data.amount + "元。");
					}
				}, {
					text: "{{ __('发送Telegram') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						window.open("https://t.me/share/url?url={{ url('/') }}&text=用户"
							+ data.user?.name + data.status + "，金额" + data.amount + "元。");
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection