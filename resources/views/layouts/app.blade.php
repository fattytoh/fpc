<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

	<title>{{ config('app.name', 'Laravel') }} @yield('title')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />	
    <link href="{{ asset('css/bootadmin/feathericons.css') }}" rel="stylesheet" />
	@yield('css')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @auth
		<script>
			const pusherKey = "{{ config('broadcasting.connections.pusher.key') }}";
			const host = "{{ url('/') }}";
			const minWithdraw = "{{ ($minWithdraw = \App\Models\Setting::where('key', 'withdraw.min')->first()) ? $minWithdraw->value : 0 }}";
			const withdrawURL = "{{ route('withdraw') }}";
		</script>
		<!-- Pusher -->
		<script src="https://js.pusher.com/7.0/pusher.min.js" defer></script>
		<!-- SweetAlert2 -->
		<script src="{{ asset('js/sweetalert2@9.js') }}" defer></script>
		<script src="{{ asset('js/withdraw.js') }}" defer></script>
		<script>
			function handleBack() {
				window.opener = self;
				window.close();
				// if not closed
				window.location.href = "{{ config('other.return_url') }}";
			}
		</script>
	@endif
	@yield('js')
    <style type="text/css">
        .god-animate{
              width:  50%;
              animation: cssAnimation 0s ease-in 5s forwards;
        }

        @keyframes cssAnimation {
                to {
                    width:0;
                    height:0;
                    overflow:hidden;
                }
            }
        main{
            height: calc( 100vh);
            overflow-y: auto;
        }

        @media(max-width:  991px){
            main{
                height: calc( 100vh);
                overflow-y: auto;
            }
        }
		
		.icon-link {
			color: white;
			font-size: 30px;
			line-height: 1;
		}
		
		.icon-link:hover {
			color: white;
			text-decoration: none;
		}
		
		* {
			touch-action: manipulation;
		}
    </style>
</head>
<body>
    <div id="app">
		@if($navbar ?? false)
        <nav class="navbar navbar-expand-md navbar-dark bg-danger shadow-sm">
			<div class="container">
				@hasSection('button')
					<a href="{{ ($previous ?? false) && url()->previous() != url()->full() && strpos(url()->previous(), 'record') == -1 ? url()->previous() : route('lobby') }}"
						class="btn btn-danger" style="margin-right: 10px;">@yield('button')</a>
				@elseif ($backRoute ?? false)
					<a class="icon-link" href="{{ $backRoute }}" style="margin-right: 10px;"><i class="feather icon-chevron-left"></i></a>
				@else
					<a class="navbar-brand" href="{{ url('/') }}">
						<img src="{{ asset('images/logo.png') }}" height="70px;"
							style="margin-top: -20px; margin-bottom: -20px;" />
					</a>
				@endguest

				@hasSection('title')
					<div class="navbar-brand">
						<div>@yield('title')@yield('title_other')</div>
					</div>
				@endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
				<!--
                    <ul class="navbar-nav ml-auto text-right">
                      
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('登入') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('注册') }}</a>
                                </li>
                            @endif
                        @else
							@php($user = Auth::user())
							<li class="nav-item">
								<a class="nav-link">余额: <b class="balance">${{ number_format($user->totalBalance, 2) }}</b> <button type="button" class="btn btn-primary withdraw">提取</button></a>
							</li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $user->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="{{ route('record.betting') }}">{{ __('下注记录') }}</a>
									<a class="dropdown-item" href="{{ route('record.topup') }}">{{ __('充值记录') }}</a>
									<a class="dropdown-item" href="{{ route('record.withdraw') }}">{{ __('提取记录') }}</a>
									<a class="dropdown-item" href="{{ route('record.invitation') }}">{{ __('邀请记录') }}</a>
									<a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('登出') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>-->
                </div>
            </div>
        </nav>
		@endif
		<main @if($navbar ?? false)  style="height: auto;" @endif>
            @yield('content')
        </main>
    </div>
	@yield('footer-js')
</body>
</html>
