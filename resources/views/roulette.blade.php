@extends('layouts.app')

@section('button', "大厅")

@section('title', $room->name)

@section('title_other', "")

@section('css')
	<style>
		
		@font-face{
			font-family: AOB;
			src: url({{  asset('fonts/AntiqueOliveBQ-NordItalic.otf') }});
		}

		*{
			font-family: 'Roboto';
		}

		.roulette-time{
			font-size:  13px;
			line-height:  0.8;
			margin-right:  5px;
		}

		.roulette-text{
			color:  #000;
		}

		.roulette-text-result  {
			padding: 5px;
		}

		.roulette-text-result i {
			font-size: 40px; 
			border-radius: 50%;
			padding: 5px;
			line-height: 1
		}

		h2{
			font-size: 40px;
			font-weight: 400;
		}

		.roulette-table-bg{
			min-height: 100vh;
		}

		.roulette-bg{
			padding: 30px;
			min-height: calc(100vh - 60px);
			background: #f0f0f0;
			border-radius: 60px;
		}

		.record {
			background-color: orange;
			border-radius: 5px;
			color: white;
			padding: 10px 0 10px 0;
		}
		.avatar {
			margin-top: auto;
			margin-bottom: auto;
			padding: 10px;
		}		
		.board {
			position: relative;
			padding: 10px;
		}
		
		@keyframes blink-cell {
		  50% {
			border: 1px yellow solid;
		  }
		}
		
		.casino-chips {
			position: relative;
			display: block;
			margin: 30px auto;
			width: 100px;
			height: 100px;
			border-radius: 50%;
			line-height: 100px;
			text-align: center;
			font-family: Helvetica;
			color: black;
			border: 13px dashed white;
		}
		
		.casino-chips:before {
			position: absolute;
			z-index: -1;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			margin: -16px;
			border-radius: 50%;
			content: '';
		}
		
		.casino-chips:after {
			position: absolute;
			z-index: -1;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			margin: 5px;
			border-radius: 50%;
			background: white;
			content: '';
		}
		
		.casino-chips-1:before {
			background: #4c4c4c;
		}
		
		.casino-chips-1:after {
			border: 3px dashed #4c4c4c;
		}
		
		.casino-chips-5:before {
			background: #2cbbc2;
		}
		
		.casino-chips-5:after {
			border: 3px dashed #2cbbc2;
		}
		
		.casino-chips-10:before {
			background: #0e99b8;
		}
		
		.casino-chips-10:after {
			border: 3px dashed #0e99b8;
		}
		
		.casino-chips-20:before {
			background: #cb3301;
		}
		
		.casino-chips-20:after {
			border: 3px dashed #cb3301;
		}
		
		.casino-chips-25:before {
			background: #cb3301;
		}
		
		.casino-chips-25:after {
			border: 3px dashed #cb3301;
		}
		
		.casino-chips-50:before {
			background: #029935;
		}
		
		.casino-chips-50:after {
			border: 3px dashed #029935;
		}
		
		.casino-chips-100:before {
			background: #653398;
		}
		
		.casino-chips-100:after {
			border: 3px dashed #653398;
		}
		
		.casino-chips-200:before {
			background: #da8002;
		}
		
		.casino-chips-200:after {
			border: 3px dashed #da8002;
		}
		
		.casino-chips-250:before {
			background: #d97e00;
		}
		
		.casino-chips-250:after {
			border: 3px dashed #d97e00;
		}
		
		.casino-chips-500:before {
			background: #679966;
		}
		
		.casino-chips-500:after {
			border: 3px dashed #679966;
		}
		
		.casino-chips-1000:before {
			background: #9a33cc;
		}
		
		.casino-chips-1000:after {
			border: 3px dashed #9a33cc;
		}
		
		.casino-chips-5000:before {
			background: #ff6766;
		}
		
		.casino-chips-5000:after {
			border: 3px dashed #ff6766;
		}
		
		.fpc {
			margin: auto;
		}
		
		.rolette {
			margin: auto;
			border-radius: 10px;
			box-shadow: 0 0 5px 1px grey;
		}

		.rolette td{
			border:  1px solid #fff;
		}
		
		.rolette .board-cell {
			height: 40px;
			width: 40px;
			text-align: center;
			font-size: 14px;
			line-height: 0.8;
			border-radius:  4px;
			font-family: 'Roboto', sans-serif;
		}
		
		.background-red {
			color: white;
			background-color: #c81527;
		}
		
		.background-blue {
			color: white;
			background-color: #0014d2;
		}
		
		.background-green {
			color: white;
			background-color: #007436;
		}
		
		.background-yellow {
			color: white;
			background-color: #e58f2a;
		}
		
		.background-black {
			color: white;
			background-color: #0e4546;
			font-size:  8px!important;
			line-height:  1.5!important;
		}

		
		.cell-range {
			font-size:  13px!important;
			line-height:  1.5!important;
		}


		.roulette-ball {
			position: relative;
			top: -5px;
		}
		
		.roulette-result {
			border-radius: 50%;
			padding: 2px;
			font-size: 11px;
			font-style: normal;
		}
		
		.chip {
			width: 50px;
			height: 50px;
		}

		.chip-on-board {
			position: absolute !important;
			height: 40px;
			width: 40px;
		}
		
		.chip-cursor {
			position: absolute !important;
			pointer-events: none;
			user-select: none;
			cursor: move;
		}
		
		.chip-board {
			position: absolute;
			pointer-events: none;
			user-select: none;
		}
		
		.chip-amount {
			color: white;
			font-size: 10px;
			position: absolute;
			pointer-events: none;
		}
		
		.chip-selected {
			border-radius: 50%;
			border: 1px red solid;
		}
		
		.selected-fpc-container {
			margin: auto;
			font-size: 0;
		}
		
		.selected-fpc {
			display: inline-block;
			border: 1px black solid;
			width: 20px;
			height: 20px;
			margin-right: 2px;
			vertical-align: middle;
			font-size: 12px;
		}
		
		.selected-chip {
			display: inline-block;
			width: 20px;
			height: 20px;
			border: 1px red solid;
			border-radius: 50%;
			vertical-align: middle;
		}

		.table-bg{
			background: url("{{ asset('images/fpc-bg.png') }}")no-repeat; 
			background-size: 100% 100%; 
			padding: 80px 60px; 
			position: relative;
		}

		.btn-roulette{
			border:  2px solid #fff;
			font-family: MyriadPro!important;
			margin-bottom: 15px;
			color:  #fff;
			font-size:  24px;
			border-radius:  20px;
		}
		
		.result-border{
			background: #fff;
			margin-bottom: 50px;
			padding:  15px;
			text-align: center;
			box-shadow: 0px 0px 15px -3px #111;
		}

		.run{
			box-shadow: inset 40px 40px 40px 40px rgba(0, 0, 0, 0.5);
		}
		@media only all and (max-width:992px) {

			.btn-roulette{
				font-size: 14px;
			}

			.roulette-table-bg{
				padding: 10px;
			}

			.roulette-bg {
				padding: 10px;
				border-radius: 10px;
				overflow: hidden;
			}

			.roulette-text-result i{
				font-size: 20px; 
				line-height: 1
			}

			.roulette-result {
				font-size: 9px;
				line-height: 10px;
				padding: 2px;
				min-height: 15px;
				width: 20px;
			}
			
			.rolette .board-cell {
				font-size: 14px;
				height: 28px;
				width: 28px;
			}
			
			.chip {
				width: 40px;
				height: 40px;
			}
			
			.chip-amount {
				font-size: 5px;
			}


			.chip-on-board {
				border: 1px black solid;
				border-radius: 50%;
				height: 25px;
				width: 25px;
			}
		}

		.gamename{
			height: 200px;
			width: 500px;
			position: absolute;
			top:  0;
			padding:  20px;
			left:  50%;
			text-align: center;
    		transform: translateX(-50%);
			z-index: 999;
			background: url('{{asset("images/gamename-bg.png")}}');
			background-size: 100% 100%;
		}

		.gamename h3{
			font-weight: 550;
			color: #f2b800;;
			font-size: 100px;
    		line-height: 0.75;
		}

		.gamename h4{
			color:  #f2b800;;
			font-size: 40px;
		}

		.title{
			font-size: 40px;
			color: #fff; 
			margin-bottom: 20px; 
		}

		.result-box{
			padding:  20px;
			background: #fff6ea;
			border:  4px solid #f2b800;
			min-height: 600px;
		}

		.result-box{
			background: #fff6ea;
			border:  2px solid #f2b800;
			margin-bottom:  30px;
		}

		.title-right{
			font-size: 32px; 
			color: #fff;
		}

		.bet-input{
			background:  url('{{asset("images/fpc-input-bg.png")}}'); 
			background-size: 100% 100%; 
			padding: 0 10px; 
			height: 40px;
			align-items: center; 
			display: flex;
			margin-bottom: 20px;
		}

		.bet-input h3{
			margin-bottom: 0;
		}

		.btn-bet{
			width: 180px;
			outline:  0;
			height: 180px;
			font-size: 100px;
			border:  0;
			line-height: 0;
			background: url('{{asset("images/fpc-open-button-bg.png")}}');
			background-size: 100% 100%;
		    margin: auto;
		    display: flex;
		    align-items: center;
		    justify-content: center;
		}

		.btn-function{
			width: 100%;
			outline:  0;
			height: 75px;
			font-size: 30px;
			border:  0;
			line-height: 0;
			background: url('{{asset("images/fpc-button-bg.png")}}');
			background-size: 100% 100%;
		    margin: auto;
		    display: flex;
		    padding:  0 25px;
		    align-items: center;
		    margin-bottom: 20px;
		}

		.bet-minmax h3{
			text-align: left;
			color: #fff ;
			font-size: 40px;
		}

		.round{
			display: flex;
		    align-items: center;
		    height: 100%;
		}

		.fpc-result-dice-box{
			padding :2.5px;
			background: #fff6ea;
			border:  2px solid #f2b800;
			background-size: 100% 100%;
		}

		.last-roll-result{
			position:  absolute; 
			top:  45%; 
			left:  55%;  
			transform: translate(-50%, -50%);
			margin:  auto; 
			border-radius:  50%; 
			width: 150px; 
			height: 150px; 
			display: flex; 
			justify-content: center; 
			align-items: center; 
			font-size: 60px
		}

		.font-bold{
			font-weight:  700;
		}

		.dice-result img{
			width: 40px;
		}

		.btn-rebet{
			background: linear-gradient(-45deg, rgba(179,6,0,1) 0%, rgba(255,0,0,1) 25%, rgba(241,90,36,1) 50%, rgba(184,0,0,1) 100%);
		}

		.btn-release{
			background: linear-gradient(-45deg, rgba(0,61,26,1) 0%, rgba(25,151,30,1) 25%, rgba(131,230,40,1) 50%, rgba(0,71,26,1) 100%);
		}

		.btn-reset{
			background: linear-gradient(-45deg, rgba(201,150,26,1) 0%, rgba(253,206,30,1) 25%, rgba(247,230,164,1) 50%, rgba(201,150,26,1) 100%);
		}
		
		.red-pack {
			position: fixed;
			top: 50%;
			left: 50%; margin-left: -50px;
			margin-top: -50px;
		}

		.modal-content{
			background:  transparent!important;
			border:  0;
		}

		.modal-body{
			text-align: center;
			color:  #fff;
			font-size: 28px;
		}

		.modal-result i {
		    color: #fff;
		    border-radius: 50%;
		    font-size: 60px;
		    margin:  auto;
		    display: flex;
		    justify-content: center;
		    align-items: center;
		    line-height: 1;
		    width: 100px;
		    height: 100px;
		}

		#board_overlay{
			color: white;
			font-size: 18px;
			display: none; 
			position: absolute; 
			left: 50%; 
			top: 50%;
			transform: translate(-50%, -50%);
			width: 100%; 
			height: 100%; 
			background: #000000AA; 
			border-radius: 30px;
		}
		
		.rebet-count {
			display: inline-block;
			background: #EEEEEEAA;
			padding: 2px 10px;
			font-size: 12px;
			color: black;
			border-radius: 50%;
		}
		
		.balance-container p {
			display: inline-block;
		}
		
		.history-collapse {
			border-radius: 12px;
			border: 1px solid rgba(0,0,0,.125);
		}

		.timer {
			font-family: AOB!important;
			font-size: 32px!important;
			padding:  0;
		}

		.timer span{
			font-family: AOB!important;
			font-size: 24px;
			padding:  0;
		}
		
		
		.bet-panel-container {
			position: fixed;
			bottom: 0;
			left: 0;
			width: 100%;
		}
		
		.bet-panel {
			box-shadow: 0 0 5px 1px grey;
			padding: 10px;
			border-radius: 0.25rem 0.25rem 0 0;
			z-index: 10;
			background-color: white;
		}
		
		.history-container {
			margin-bottom: 50px;
			background-color: white;
		}

		@media(max-width:  991px){
			#board_overlay{
				border-radius: 20px;
			}

			.roulette-text {
				font-size: 20px;
			}

			.gamename{
				padding: 5px 10px 10px 10px;
				height: 80px;
				width: 194px;
				
			}

			.gamename h3{
				font-size: 40px;
				text-align: center;
			}

			.gamename h4{
				 font-size: 16px;
			}

			.title{
				font-size: 20px;
				margin-bottom: 5px;
			}

			.title-right{
				font-size: 20px; 
				color: #fcee21;
			}

			.bet-input{
				height: 100%;
			}

			.bet-input h3{
				font-size: 16px;
			}

			.btn-bet{
				width: 100px;
				height: 100px;
				font-size: 60px;
			}

			.btn-function {
				height:  35px;
				padding:  0 12px;
				margin-bottom: 5px;
			}

			.btn-function img{
				height:  20px!important;
				margin-top: 0!important;
				margin-right: 5px!important;
			}

			.btn-function p{
				font-size: 14px;
				color:  #fff;
				margin-bottom: 0;
			}

			.result-box{
				padding: 10px;
				min-height: 250px;
			}

			.bet-minmax h3{
				font-size: 18px;
			}

			.last-roll-result{
				position:  absolute; 
				top:  45%; 
				left:  55%;  
				transform: translate(-50%, -50%);
				margin:  auto; 
				border-radius:  50%; 
				width: 60px; 
				height: 60px; 
				display: flex; 
				justify-content: center; 
				align-items: center; 
				font-size: 20px
			}

			.table-bg{
				padding: 25px 21px; 
			}

			.dice-result img{
				width: 13px;
			}

			.result-border{
				padding: 5px;
				margin-bottom: 15px;
			}

			.timer{
				font-size: 32px!important;
				padding:  0;
			}

			h4{
				font-size: 18px;
			}
		
			.history-container {
				margin-bottom: 120px;
			}
		}

		@media(orientation: portrait) {
		    .portrait{
		    	display: block
		    }

		    .landscape{
		    	display: none;
		    }
		}

		@media screen and (max-device-width: 992px) and (orientation: landscape) {
			.landscape{
				text-align: center;
		    	display: block;
		    	min-height: calc(100vh - 56px);
		    	padding:  20px;
		    }

		    .landscape div{
			    top: 50%;
			    position: absolute;
			    left:  50%;
			    transform: translate(-50%, -50%)!important;
		    }

		    .landscape div img{
		    	width: 80px; 
		    	display: block;
		    	margin:  20px auto;
		    }

		    .portrait{
		    	display: none;
		    }
		}

		@media(min-width:  992px){
			.landscape{
		    	display: none;
		    }
		}

		/* chip */

		.chip-outer{
		  width: 55px;
		  height: 55px;
		  border-radius: 50%;
		  position: relative;
		  box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
		  -moz-box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
		  -webkit-box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
		}

		.chip-inner {
		    float: left;
		    width: 40px;
		    height: 40px;
		    border-radius: 50%;
		    -webkit-border-radius: 150px;
		    -moz-border-radius: 150px;
		    position: absolute;
		    color: #fff;
		    top:  50%;
		    left: 50%;
		    border: 1px solid #fff; 
		    transform: translate(-50%, -50%);
		}

		.chip-inner div {
		    text-align: center;
		    line-height: 0;
		    font-size: 100%;
		    position: absolute;
		    top:  50%;
		    left: 50%;
		    transform: translate(-50%, -50%);
		}
		
		.chip-color-01 {
			background-color: #bf2326;
		}
		.chip-color-01 .chip-inner {
			background-color: #bf2326;
		}
		.chip-color-05 {
			background-color: #396832;
		}
		.chip-color-05 .chip-inner {
			background-color: #396832;
		}
		.chip-color-1 {
			background-color: #3d489e;
		}
		.chip-color-1 .chip-inner {
			background-color: #3d489e;
		}
		.chip-color-5 {
			background-color: #FF9100;
		}
		.chip-color-5 .chip-inner {
			background-color: #FF9100;
		}
		.chip-color-10 {
			background-color: #7255a4;
		}
		.chip-color-10 .chip-inner {
			background-color: #7255a4;
		}
		.chip-color-50 {
			background-color: #33302f;
		}
		.chip-color-50 .chip-inner {
			background-color: #33302f;
		}
		.chip-color-border {
			background-image: url("{{ asset('images/chip.svg') }}");
		}

		@media(max-width:  768px) {
			.roulette-ball {
				position: relative;
				top: -2px;
				margin-left:  5px;
			}

			/* chip */

			.chip-outer{
			  width: 45px;
			  height: 45px;
			  border-radius: 50%;
			  position: relative;
			  box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
			  -moz-box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
			  -webkit-box-shadow: inset 0 0 1px 1px rgba(0,0,0, 0.1), 0 0 5px 1px rgba(90,90,90, 0.1);
			}

			.chip-inner {
				float: left;
				width: 35px;
				height: 35px;
				border-radius: 50%;
				-webkit-border-radius: 150px;
				-moz-border-radius: 150px;
				position: absolute;
				color: #fff;
				top:  50%;
				left: 50%;
				border: 1px solid #fff; 
				transform: translate(-50%, -50%);
			}

			.chip-inner div {
				text-align: center;
				line-height: 1;
				font-size: 80%;
				position: absolute;
				top:  50%;
				left: 50%;
				transform: translate(-50%, -50%);
			}
		}
		
		.btn-outline-dark-blue {
			color: #00008b;
			border-color: #00008b;
		}
		
		.btn-outline-dark-blue:active {
			color: #fff;
			background-color: #00008b;
			border-color: #00008b;
		}
		
		.btn-outline-dark-blue:hover {
			color: #fff;
			background-color: #00008b;
			border-color: #00008b;
		}
		
		.history-text {
			padding: .375rem 1rem;
			color: #EB6864;
		}
		
		.history-result {
			font-size: 9px;
			line-height: 10px;
			padding: 2px;
			min-height: 15px;
			white-space: pre;
			display: inline-block;
			border-radius: 50%;
			padding: 2px;
			font-size: 11px;
			font-style: normal;
			vertical-align: middle;
		}
	</style>
@endsection

@section('content')
<div class="portrait" style="background: #f0f0f0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 mx-auto">
				<div class="roulette-bg">
					<div class="row">
						<div class="col-12 mb-2 p-0">
							<div class="row mx-auto">
								@if ($room->is_standalone)
								@php($last10 = $room->endedBettings()->where('standalone_user', $user->id)
									->limit(24)->get()->sortBy('id')->values())
								@else
								@php($last10 = $room->endedBettings()->limit(24)->get()->sortBy('id')->values())
								@endif
								<div id="result_left" class="col-3 px-0 text-center">
									@for($i = 0; $i < 12; $i++)
									@php($last = $last10->get($i))
									<div id="result_{{ $last ? $last->id : '' }}" class="row result">
										<div class="d-flex mx-auto">
										@if($last)
											<div class="roulette-time text-muted">
												{{ date('H:i', strtotime($last->version)) }}
											</div>
										@else
											<div class="roulette-time text-muted">
												{{ $last10->last() ?  date('H:i', strtotime($last10->last()->version) + $room->interval * ($i - $last10->count() + 1)) : '' }}
											</div>
										@endif
										@if($last && $result = $last->results()->first())
											<div class="roulette-result roulette-ball">
												<i class="background-{{ $result->rouletteColor }} roulette-result">{{ str_pad($result->value, 2, "0", STR_PAD_LEFT) }}</i>
											</div>
										@else
											<div class="roulette-result roulette-ball">
												<i class="invisible roulette-result">--</i>
											</div>
										@endif
										</div>
									</div>
									@endfor
								</div>
								<div class="col-6 px-0">
									<div class="text-center my-auto"  style="color:  #fff;border-radius: 10px;background: linear-gradient(90deg, rgba(235,33,57,1) 0%, rgba(147,40,30,1) 100%);">
									 	<div class=" py-3">
									 		<div id="counter_container" class="p-sm-3">
											 	<h4 style="font-size: 16px; font-family: 'Roboto', sans-serif;" class="mb-1">Next draw is</h4>
											 	<p id="current_version" class="mb-0" style="font-size: 16px; font-family: 'Roboto'">{{ $room->current ? $room->current->version : '' }}</p>
											 	<div class="roulette-text timer mb-3" style="color: #fff;font-family: AOB;">
													<span id="minute">--</span><span style="font-size: 16px">m</span><span class="ml-1" id="second">--</span><span style="font-size: 16px">s</span>
												</div>
											 </div>
										 	 <div id="open_container" class="p-sm-3 p-2" style="display: none;">
												<h4 id="open_result_title">Drawing Result</h4>
											 	<p></p>
												<div id="open_result" style="font-size: 52px; font-family: 'AOB'"></div>
											 </div>
											 <div>
												<p style="font-size: 10px; font-family: 'AOB'" class="mb-0">2D Lucky Strike</p>
											 </div>
									 	</div>
									</div>
								</div>
								<div id="result_right" class="col-3 px-0">
									@for(; $i < 24; $i++)
									@php($last = $last10->get($i))
									<div id="result_{{ $last ? $last->id : '' }}" class="row">
										<div class="d-flex mx-auto">
										@if($last)
											<div class="roulette-time text-muted">
												{{ date('H:i', strtotime($last->version)) }}
											</div>
										@else
											<div class="roulette-time text-muted">
												{{ $last10->last() ? date('H:i', strtotime($last10->last()->version) + $room->interval * ($i - $last10->count() + 1)) : '' }}
											</div>
										@endif
										@if($last && $result = $last->results()->first())
											<div class="roulette-result roulette-ball ">
												<i class="background-{{ $result->rouletteColor }} roulette-result">{{ str_pad($result->value, 2, "0", STR_PAD_LEFT) }}</i>
											</div>
										@else
											<div class="roulette-result roulette-ball">
												<i class="invisible roulette-result">--</i>
											</div>
										@endif
										</div>
									</div>
									@endfor
								</div>
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-12">
							<input type="hidden" id="betting_id" value="{{ $room->current ? $room->current->id : '' }}" />
							<div class="row" style=" overflow-y: auto; padding-bottom: 20px;">
								<div class="col-12">
									<div id="board_container" class="row">
										<div style="padding: 0; margin: auto; position: relative;">
											@php($rateGroups = $room->rateGroups)
											@php($number = $rateGroups->where('group', 'number')->first())
											@php($single = $rateGroups->where('group', 'single')->first())
											@php($color = $rateGroups->where('group', 'color')->first())
											@php($tenRange = $rateGroups->where('group', 'ten_range')->first())
											@php($twentyRange = $rateGroups->where('group', 'twenty_range')->first())
											<table id="board" class="board rolette">
												<tbody>
													<tr>
														<td class="board-cell background-black" style="border-top-left-radius: 10px; border-top: 0; border-left: 0;">
														</td>
														@foreach(['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'] as $nWord)
														<td id="{{ $nWord }}" class="board-cell background-black" onclick="addRecord(this, {{ $number->id }})">
															{{ $nWord }}
														</td>
														@endforeach
														<td class="board-cell background-black" style="border-top-right-radius: 10px; border-top: 0; border-right: 0;">
														</td>												
													</tr>
													@for($i = 0; $i < 10; $i++)
													<tr>
														<td id="{{ $i * 10 }}-{{ ($i * 10) + 9 }}" class="board-cell background-black"  onclick="addRecord(this, {{ $tenRange->id }})">
															{{ $i * 10 }}-{{ ($i * 10) + 9 }}
														</td>
														@for($j = 0; $j < 10; $j++)
														<td id="{{ ($i * 10) + $j }}" class="board-cell background-{{
															$i < 5 && 10 - $j - $i - 1 > 0 && $j - $i + 1 > 0 ? 'yellow' : (
															$j > 4 && $j - $i > 0 ? 'green': (
															$i > 4 && 10 - $j - $i - 1 < 0 && $j - $i - 1 < 0 ? 'blue': 'red'))
														}}" onclick="addRecord(this, {{ $single->id }})">
															{{ str_pad(($i * 10) + $j, 2, "0", STR_PAD_LEFT) }}
														</td>
														@endfor
														@if($i % 2 == 0)
														<td id="{{ $i * 10 }}-{{ ($i * 10) + 19 }}" rowspan="2" class="board-cell background-black cell-range" onclick="addRecord(this, {{ $twentyRange->id }})">
															{{ str_pad($i * 10, 2, "0", STR_PAD_LEFT) }}<br />{{ str_pad(($i * 10) + 19, 2, "0", STR_PAD_LEFT) }}
														</td>
														@endif
													</tr>
													@endfor
													<tr class="bigger-border">
														@foreach(['red', 'blue', 'green', 'yellow'] as $cWord)
														<td id="{{ $cWord }}" colspan="3" class="board-cell background-{{ $cWord }}" onclick="addRecord(this, {{ $color->id }})"
															style="{{ 
																$cWord == 'red' ? 'border:  1px solid #fff; border-bottom-left-radius: 10px; border-bottom: 0; border-left: 0;' : 
																($cWord == 'yellow' ? 'border-bottom-right-radius: 10px; border-bottom: 0; border-right: 0;' :
																'border:  1px solid #fff;')
															}}">
															{{ $cWord }}
														</td>
														@endforeach
													</tr>
												</tbody>
											</table>
											<div id="pending_record" class="col-12" style="position: absolute; left: 0; top: 0;"></div>
											<div id="board_overlay">
												<div id="board_overlay_text" style="color: white; left: 50%; top: 50%; position: absolute; user-select: none; font-size: 18px; transform: translateX(-50%);"></div>
											</div>
										</div>
									</div>
									<div id="chip_container" class="row mt-3 mb-2 mt-xl-3 mb-xl-3">
										<div class="col-12 col-sm-10 mx-auto">
											<div class="row">
												@foreach(['0.1', '0.5', '1', '5', '10', '50'] as $cAmount)
												<div class="col-2 px-0">
													<div onmouseup="createDragChip(this, {{ $cAmount }})"
													ontouchstart="createDragChip(this, {{ $cAmount }}, false)"
													class="chip-outer chip-color-{{ str_replace('.', '', $cAmount) }} chip-color-border m-auto">
														<div class="chip-inner">
														  <div>{{ $cAmount }}</div>
														</div> 
													</div>
												</div>
												@endforeach
											</div>
										</div>
									</div>

									<div class="row mt-3 history-collapse history-container">
										<div class="col-12 px-0" style="margin-top: 10px;">
											<h4 onclick="onCollapse()" class="history-text" data-toggle="collapse" href="#collapse"
												role="button" aria-expanded="false" aria-controls="collapse">Bet List 
												<i id="history_icon" class="feather icon-chevron-down" style="float: right;"></i>
											</h4>
											<div id="collapse" class="collapse">
											  <div class="card card-body" style="background-color: inherit;">
											    <div id="history_list"></div>
												<div id="no_history">No bet history.</div>
											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</div>
<!-- sound effect -->
<audio id="chip_audio" preload>
	<source src="{{ asset('audios/chip.mp3') }}" type="audio/mp3"></source>
</audio>
<!-- result -->
@include('layouts.result-modal', ['title' => "The number is: "])				
<!-- panel -->
<div id="betConfirmation" class="bet-panel-container">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 mx-auto bet-panel">
				<div id="toastContainer" class="row" style="display: none;">
					<div class="col-12">
						<div id="toastMessage"></div>
					</div>
				</div>
				<div class="row" style="margin-bottom: 5px;">
					<div class="col-6 text-right font-bold" style="font-family: 'Roboto', sans-serif;">
						Total Bet: <b id="betAmount" style="font-family: 'Roboto', sans-serif;">0.00</b>
					</div>
					<div class="col-6 text-left font-bold" style="font-family: 'Roboto', sans-serif;">
						<div class="d-flex my-auto">
							<img src="{{ asset('images/balance.svg') }}" width="20"/> <b class="balance ml-2" style="font-family: 'Roboto', sans-serif;">{{ number_format($user->totalBalance, 2) }}</b>
						</div>
						
					</div>
				</div>
				<div class="row justify-content-center m-0">
					<div class="col-4 p-1">
						<button id="rebet" type="button" onclick="reBet()" class="btn btn-block btn-outline-dark-blue" disabled style="white-space: pre; font-family: 'Roboto', sans-serif;">Rebet <div id="rebet_count" style="display: none;">x1</div></button>
					</div>
					<div class="col-4 p-1">
						<button id="confirm"style="font-family: 'Roboto', sans-serif;" type="button" onclick="confirmBet()" disabled class="btn btn-block btn-success">Confirm</button>
					</div>
					<div class="col-4 p-1">
						<button id="clear"style="font-family: 'Roboto', sans-serif;" type="button" onclick="clearBet()" disabled class="btn btn-block btn-outline-danger">Reset</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="landscape">
	<div>
		<img src="{{ asset('images/rotate-to-portrait.png') }}">
		<p>Please rotate your mobile into portrait mode.</p>
	</div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
	    const channelName = "presence-room.{{ $room->id }}";
		const eventName = "betting.updated";
		const gameKey = "{!! $room->game->key !!}";
		const fpcOrders = {!! json_encode($fpc_order) !!};
		let startDate = {{ !$room->is_standalone && $room->current ? $room->current->ended_at->setTimezone('UTC')->timestamp * 1000 : 0 }};
		const chipAmounts = [0.1, 0.5, 1, 5, 10, 50];
		@php($pre = $user->bettingRecords()
			->whereHas('betting', function($bQuery) use ($room) {
				$bQuery->where('room_id', $room->id);
			})
			->groupBy(['group'])
			->select(['group'])
			->limit(5)
			->pluck('group'))
		const histories = {!! $user->bettingRecords()
			->whereIn('group', $pre)
			->orderByDesc('betting_id')
			->select(['id', 'amount', 'won', 'group', 'play_type', 'play_group', 'play_option', 'play_name', 'betting_id', 'created_at'])
			->get();
		!!};
		let isCollapse = false;		
		let pendingRecords = [];
		let rebet = 0;
		let isConfirmed = false;
		const numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
		
		function clearRecord() {
			const id = $("#betting_id").val();
			if(id) {
				$.ajax({
					url: "{{ route('room.clear', ['id' => $room->id]) }}",
					data: {
						betting_id: id,
					},
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "POST"
				}).done(function(res) {
					updateBalance(res?.user);
					updateBetAmount();
					showHidePanel(false);
				}).fail(function(err) {
					Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
				});
			}
		}
		
		function loadRecord() {
			const id = $("#betting_id").val();
			if(id) {
				$.ajax({
					url: "{{ route('room.record', ['id' => $room->id]) }}",
					data: {
						betting_id: id,
					},
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "POST"
				}).done(function(records) {
					loadRecordProcess(records, "play_option");
				}).fail(function(err) {
					Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
				});
			}
		}		
		
		function loadHistoryRecord(id) {
			$.ajax({
				url: "{{ route('room.record', ['id' => $room->id]) }}",
				data: {
					betting_id: id,
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: "POST"
			}).done(function(records) {				
				const grouped = [];
				
				records.forEach(data => {
					const group = data.group;
					if (!grouped[group]) {
						grouped[group] = [];
					}
					grouped[group].push(data);
				});
				Object.keys(grouped).forEach(key => {
					// let html = "";
					let total = 0;
					let won = 0;
					grouped[key].forEach(data => {
						const wonAmount = parseFloat(data.won);
						if (wonAmount > 0) {
							let bgColor = data.roulette_color;
							let isRectBorder = false;
							if (!bgColor) {
								isRectBorder = true;
								if (["red", "blue", "green", "yellow"].indexOf(data.play_option) > -1) {
									bgColor = data.play_option;
								} else {
									bgColor = "black";
								}
							}
							html += "<div class='row'>" +
										"<div class='col-12'>" +
											"<div class='mb-0'>" + showHistoryRouletteBall(data.play_option, bgColor, isRectBorder) + ": " + data.won + "</div>" +							
										"</div>" +
									"</div>";
						}
						total += data.amount;
						won += wonAmount;
					});
					if (won > 0) {
						$("#history_winning_" + key).show();
					}
					$("#history_balance_container_" + key).show();
					$("#history_result_" + key).html(html);
					$("#history_won_" + key).text("Total Won: " + won.toFixed(2));
					$("#history_balance_" + key).html("Win/Lose: <b style='color: " + (total - won > 0 ? 'red': 'green') + ";'>" + (total - won).toFixed(2) + "</b>");
				});
			}).fail(function(err) {
				Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
			});
		}
		
		function loadRecordProcess(records, optionField) {
			$("#pending_record").empty();
			records.forEach(function(record) {
				const ele = document.getElementById(record[optionField]);
				if (ele) {
					addRecordProcess(ele, record.amount, record.room_rate_id, false);
				}
			});
		}
		
		function loadTotalWon(id) {
			$.ajax({
				url: "{{ route('room', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					betting_id: id,
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				// set total won
				//$("#total_cost").text(res.cost.toFixed(2));
				//$("#total_won").text(res.won.toFixed(2));
				if (res.cost > 0) {
					$('#result_modal').modal({show:true});
					$("#result_panel").show();
					const balance = res.won - res.cost;
					$("#total_sum").css("color", balance > 0 ? "green" : "red");
					$("#total_sum").text((balance > 0 ? "+" : "") + balance.toFixed(2));
				}
			}).fail(function(err) {
				Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
			});
		}

		function updateBalance(user) {
			$(".balance").text(user?.total_balance);
		}
		
		function createDragChip(target, cAmount, createVisual = true) {
			const selectedChips = document.getElementsByClassName("chip-selected");
			
			for (let selectedChip of selectedChips) {
				$(selectedChip).removeClass("chip-selected");
			}
			
			if (!$(target).hasClass('chip-selected')) {
				$(target).addClass("chip-selected");
			}
			
			const dragChips = document.getElementsByClassName("chip-cursor");
			let dragChip = dragChips.length > 0 ? dragChips[0] : null;
			if (dragChip) {
				$(dragChip).removeClass("chip-color-" + dragChip.dataset.value.toString().replace(".", ""));
				$(dragChip).addClass("chip-color-" + cAmount.toString().replace(".", ""));
				dragChip.dataset.value = cAmount;
				dragChip.children[0].children[0].innerText = cAmount;
				return;
			}			
			createChip(target.clientY - 20, target.clientX - 20, cAmount, createVisual, true, null);
			updateConfirmButton("Release");
		}
		
		function createChip(top, left, cAmount, createVisual, isCursor, amountId, height, width) {	
			const chipOuter = document.createElement("div");
			chipOuter.dataset.value = cAmount;
			chipOuter.className = "chip-outer chip-color-" + cAmount.toString().replace(".", "") + " "
					+ (isCursor ? "chip-cursor chip-color-border" : "chip-on-board chip-board");
			const chipInner = document.createElement("div");
			chipInner.className = "chip-inner";
			const chipValue = document.createElement("div");
			chipValue.style.fontSize = "12px";
			chipValue.innerText = cAmount;
			chipInner.appendChild(chipValue);
			chipOuter.appendChild(chipInner);
			if (!isCursor) {
				const svgRatio = 0;
				const ratio = 40 / 55;
				const size = height > width ? width : height;
				const padding = 1;
				const outerSize = size - padding;
				chipOuter.style.left = (left + ((Math.abs(width - size) + padding) / 2)) + "px";
				chipOuter.style.top = (top + ((Math.abs(height - size) + padding) / 2)) + "px";
				chipOuter.style.height = outerSize + "px";
				chipOuter.style.width = outerSize + "px";
				chipOuter.className += " chip-" + amountId;
				const url = "{{ asset('images/chip.svg') }}";
				chipOuter.style.backgroundImage = 'url("' + url + '")';
				chipInner.style.height = outerSize * ratio + "px";
				chipInner.style.width = outerSize * ratio + "px";
				document.getElementById("pending_record").appendChild(chipOuter);
			} else {
				if (createVisual) {
					chipOuter.style.left = left + "px";
					chipOuter.style.top = top + "px";
					document.body.addEventListener('mousemove', moveDragChip);
				} else {
					chipOuter.style.display = "none";
				}
				document.body.appendChild(chipOuter);
			}
		}
		
		function showHidePanel(show) {
			const panelEle = document.getElementById("betConfirmation");
			panelEle.style.display = show ? "block" : "none";
		}
		
		function updateBetAmount() {
			const amount = pendingRecords.reduce((pv, item) => pv + item.amount, 0);
			const amountEle = document.getElementById("betAmount");
			amountEle.innerText = (amount * (rebet + 1)).toFixed(2);
		}
		
		function updateConfirmButton(text, disabled = false) {
			const btn = document.getElementById("confirm");
			btn.disabled = disabled;
			btn.innerText = text;
			
			const btn2 = document.getElementById("rebet");
			btn2.disabled = disabled;
			
			const btn3 = document.getElementById("clear");
			btn3.disabled = disabled;
		}
		
		function addRecord(target, roomRateId) {
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			if (!dragChip) {
				return;
			}
			let amount = parseFloat(dragChip.dataset.value);
			addRecordProcess(target, amount, roomRateId, true);
		}
		
		function addRecordProcess(target, amount, roomRateId, isNew) {
			let value = target.innerText;
			let exist;
			if (isNew) {
				// find existing by value
				const index = pendingRecords.findIndex(x => x.value === value);
				if (index > -1) {
					exist = pendingRecords[index];
					exist.amount += amount;
					// pre
					//const higherAmount = chipAmounts.find(x => x === exist.amount);
					// group chip on board
					//if (higherAmount) {
					//	exist.chips[higherAmount] += 1;
						// $(".chip-board-" + value).remove();
					//}			
					// update
					//exist.chips[value] = exist.chips[value] ? exist.chips[value] + 1: 1;
					pendingRecords[index] = exist;
					const chipOuter = $(".chip-" + value);
					let cIndex = chipAmounts.length - 1;
					while (chipAmounts[cIndex] > exist.amount.toFixed(1)) {
						cIndex--;
					}
					if (cIndex > 0 &&
						!chipOuter.hasClass("chip-color-" + chipAmounts[cIndex].toString().replace(".", ""))) {
						chipOuter.removeClass("chip-color-" + chipAmounts[cIndex - 1].toString().replace(".", ""));
						chipOuter.addClass("chip-color-" + chipAmounts[cIndex].toString().replace(".", ""));
					}
					const chipInner = chipOuter[0].children[0].children[0];
					const textSize = window.innerWidth > 600 ? 12 : 8;
					const textLength = exist.amount.toString().length;
					chipInner.style.fontSize = (textSize - ((textLength - 2) * 1)) + "px";
					chipInner.innerText = Math.round(exist.amount * 10) / 10;
				} else {
					// add
					//let chips = {};
					//chips[value] = 1;
					pendingRecords.push({
						amount: amount,
						room_rate_id: roomRateId,
						value: value,
					//	chips: chips
					});
				}
				duplicateChipAudio();		
			}
			if (!exist) {
				const board = document.getElementById("board");
				const defaultTop = board.offsetTop + target.clientTop + target.offsetTop + target.scrollTop;
				const defaultLeft = board.offsetLeft + target.clientLeft + target.offsetLeft + target.scrollLeft;
				if (!isCollapse) {
					createChip(defaultTop, defaultLeft, amount, true, false, target.innerText, target.clientHeight, target.clientWidth);
				}
			}			
			updateConfirmButton("Confirm");
			updateBetAmount();
		}
		
		function moveDragChip(e) {
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			
			if (!dragChip) {
				return;
			}
			dragChip.style.left = e.clientX - 20 + "px";
			dragChip.style.top = e.clientY - 20 + "px";
		}
		
		function releaseDragChip() {			
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			
			if (dragChip) {
				document.body.removeChild(dragChip);
				document.body.removeEventListener('mousemove', moveDragChip);
			}
		}
		
		function clearChip() {
			const pending = document.getElementById("pending_record");
			if (pending) {
				pending.innerHTML = '';
			}
		}
		
		function resetBet() {
			pendingRecords = [];
			clearChip();
			updateBetAmount();
			updateConfirmButton("Release", true);
		}
		
		function betRestart() {
			$("#open_container").animate({ height: "toggle" }, 500);
			$("#counter_container").animate({ height: "toggle" }, 500);
			$("#open_result_title").text("Drawing Result");
			restartTimer();
			loadRecord();
			resetBet();
		}
		
		function confirmBet() {
			if (isConfirmed) {
				updateConfirmButton("Confirm", true);
				clearReBet();
				resetBet();
				isConfirmed = false;
				return;
			}
			
			if (pendingRecords.length > 0) {
				Swal.fire({
					text: "Are you sure want to spent "
						+ pendingRecords.reduce((pv, x) => pv + x.amount, 0).toFixed(2)
						+ " on bet? you cannot reset after confirm.",
					showCancelButton: true,
					showCloseButton: true,
					confirmButtonText: "Confirm",
					cancelButtonText: "Cancel"
				}).then(result => {
					if (result.isConfirmed) {
						const id = $("#betting_id").val();
						if(!id) {
							return showToastMessage("no more bet.", "warning");
						}
						
						$.ajax({
							url: "{{ route('room.bet', ['id' => $room->id]) }}",
							type: "POST",
							data: {
								rebet: rebet,
								records: pendingRecords,
								betting_id: id
							},
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						}).done(function(res) {
							releaseDragChip();
							isConfirmed = true;
							// updateConfirmButton("New Bet");
							updateBetAmount();
							updateBalance(res.user);
							showToastMessage("Bet Confirmed.");
							addHistories(res.record, true);
						}).fail(function(err) {
							showToastMessage(err?.responseJSON?.message || err.responseText, "danger");
						});
					}
				});
			} else {
				releaseDragChip();				
				updateConfirmButton("Confirm", true);
			}
		}
		
		function clearBet() {
			clearReBet();
			resetBet();
			releaseDragChip();
			//clearRecord();
		}
		
		let toastTimeout;
		
		function showToastMessage(message, status = "success") {
			const toastContainer = document.getElementById("toastContainer");
			const toastMessage = document.getElementById("toastMessage");
			
			toastMessage.innerText = message;
			toastMessage.className = "alert alert-" + status;
			
			if (toastTimeout) {
				clearTimeout(toastTimeout);
			} else {
				toastContainer.style.display = "block";
			}
			
			toastTimeout = setTimeout(() => {
				toastTimeout = null;
				toastContainer.style.display = "none";
			}, 1000);
		}
		
		function showNoMoreBet() {
			// show 
			showBoard("No more bet");
		}
		
		function showBoard(message) {
			// show 
			$('#board_overlay').show();
			$("#board_overlay_text").text(message);
		}
		
		let animateTotalDuration = 2000;
		let animateRemainDuration;
		let defaultAnimateInterval = 50;
		let animateInterval;
		
		function calculateAnimateResult(finalValue) {			
			if (animateRemainDuration > 0) {
				animateRemainDuration -= animateInterval;
				const value = Math.floor(Math.random() * 99);
				animateResult(value);
				$("#open_result").html(value);
				setTimeout(function() {
					calculateAnimateResult(finalValue);
				}, animateInterval);
			} else {
				$("#open_result_title").text("Final Result");
				$("#open_result").html(finalValue);
				animateResult(finalValue);
			}
		}
		
		function animateResult(value) {			
			const results = $(".board-cell");
			results.addClass("run");
			const result = $("#" + value);
			result.removeClass("run");
		}
		
		function showResult(id, resultHtml, { value, roulette_color }) {
			// run animation
			animateInterval = defaultAnimateInterval;
			animateRemainDuration = animateTotalDuration;
			$("#counter_container").animate({ height: "toggle" }, 500);
			$("#open_container").animate({ height: "toggle" }, 500);
			calculateAnimateResult(value);
			setTimeout(function() {
				$("#last_result_modal").html(resultHtml);
				$("#last_result").html(resultHtml);
				$(".history-" + id).html("<div class='btn btn-primary p-0 px-1' style='font-size: 12px'><b>Confirmed</b></div>");
				loadHistoryRecord(id);				
				const startValue = Math.floor(value / 10) * 10;
				let html = showHistoryRouletteBall((value).toString().padStart(2, "0"), roulette_color, false)
					+ " / " + showHistoryRouletteBall(roulette_color, roulette_color, true)
					+ " / " + showHistoryRouletteBall(numbers[value % 10], "black", true)
					+ " / " + showHistoryRouletteBall(startValue + "-" + (startValue + 9), "black", true)
					+ " / " + showHistoryRouletteBall(startValue + "\n" + (startValue + 19), "black", true);
				$(".history-result-" + id).html(html);
				// $(".history-result-" + id).show();
				$(".history-seperator-" + id).show();
				loadTotalWon(id);
				setTimeout(function() {
					$('#result_modal').modal('hide');
					betRestart();
					const results = $(".board-cell");			
					results.removeClass("run");
					showBoard("Place your bet");
					setTimeout(() => {
						$('#board_overlay').hide();
					}, 1000);
				}, 5000);
			}, animateTotalDuration + 500);
		}
		
		function createRedPack() {
			const div = document.createElement("div");
			div.className = "red-pack";
			div.addEventListener('click', claimRedPack);
			const img = document.createElement("img");
			img.src = "{{ asset('images/redpack.jpg') }}";
			img.width = "100";
			img.height = "100";
			div.append(img);
			document.body.append(div);
		}
		
		function claimRedPack() {
			$.ajax({
				url: "{{ route('room.redpack', ['id' => $room->id]) }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				updateBalance(res.user);
				// play sound?
				$(".red-pack").remove();
				setTimeout(() => createRedPack(), res.interval * 1000);
			}).fail(function(err) {
				showToastMessage(err?.responseJSON?.message || err.responseText, "danger");
			});
		}
		
		function addHistories(records, isTop = false) {
			const grouped = [];
			
			records.forEach(data => {
				const group = data.group;
				if (!grouped[group]) {
					grouped[group] = [];
				}
				grouped[group].push(data);
			});
			Object.keys(grouped).forEach(key => {
				addHistory(grouped[key], isTop);
			});
		}
		
		function addHistory(datas, isTop = false) {
			const history = datas[0];
			const cDate = moment(history.created_at);			
			let html = "<div class='card mb-3'>" +
				"<div class='card-body'>" +
					"<div class='row mb-2'>" +
						"<div class='col-7 col-sm-10'>" +
							"<div class='text-secondary'>" + cDate.format("Do MMM • HH:mm") + "</div>" +
						"</div>" +
						"<div class='col-5 col-sm-2 history-"  + history.betting?.id + "' style='text-align: right'>" +
							(history.result === -1 ?
							"<div class='btn btn-warning p-0 px-1' style='font-size: 12px'><b>Committed</b></div>" :
							"<div class='btn btn-primary p-0 px-1' style='font-size: 12px'><b>Confirmed</b></div>") +
						"</div>" +
					"</div>" +
					"<div>" +
						"<div class='row mb-2'>" +
							"<div class='col-12'>" +
								"<p>Ref #" + history.group + "</p>" +
							"</div>" +
						"</div>";
			// let icons = "";
			// datas.forEach(data => {
				// let bgColor = data.roulette_color;
				// if (!bgColor) {
					// if (["red", "blue", "green", "yellow"].indexOf(data.play_option) > -1) {
						// bgColor = data.play_option;
					// } else {
						// bgColor = "black";
					// }
				// }
				// icons += showHistoryRouletteBall(data.play_option, bgColor, data.play_option.length !== 2);
			// });
			html += "<div class='row'>" +
						"<div class='col-12'>" +
							"<div class='mb-2'>Time: " + history.betting?.version + "</div>" +
							"<hr />" +
							"Bet List: " +
							// "<div>" + icons + "</div>" +					
						"</div>" +
					"</div>";
			let total = 0;
			let won = 0;
			datas.forEach(data => {
				html += "<div class='row'>" +
							"<div class='col-12'>" +
								"<div>" + showHistoryRouletteBall(data.play_option, "", false) + "=" + data.amount.toFixed(2) + "</div>" +							
							"</div>" +
						"</div>";
				total += data.amount;
				won += data.won ? parseFloat(data.won) : 0;
			});
			
			html += "<div class='row mt-2'>" +
						"<div class='col-12'>" +
							"<div>Total Bet: " + total.toFixed(2) + "</div>" +
						"</div>" +
					"</div>";
			
			let results = "";
			if (history.betting?.results.length > 0) {
				const result = history.betting?.results[0];
				const startValue = Math.floor(result.value / 10) * 10;
				results += showHistoryRouletteBall(result.value, result.roulette_color, false)
					+ " / " + showHistoryRouletteBall(result.roulette_color, result.roulette_color, true)
					+ " / " + showHistoryRouletteBall(numbers[result.value % 10], "black", true)
					+ " / " + showHistoryRouletteBall(startValue + "-" + (startValue + 9), "black", true)
					+ " / " + showHistoryRouletteBall(startValue + "\n" + (startValue + 19), "black", true);
			}
			html += "<div class='row history-seperator-" + history.betting?.id + "'>" +
						"<div class='col-12' style='display: " + (history.result > -1 ? 'block' : 'none') + ";'>" +
							"<hr />" +
							"<div>Result: </div>" +
							"<div class='istory-result-" + history.betting?.id + "'>" + results + "</div>" +
						"</div>" +
					"</div>";
			html += "<div id='history_winning_" + history.group + "' class='row mt-2'  style='display: "
				+ (history.result > -1 && won > 0 ? 'block' : 'none') + ";'>" +
					"<div class='col-12'>" +
						"<div>Winning Awards：</div>" +
					"</div>" +
			"</div>";
			html += "<div id='history_result_" + history.group + "'>";
			if (history.result > -1 && won > 0) {						
				datas.forEach(data => {
					const wonAmount = parseFloat(data.won);
					if (wonAmount > 0) {
						let bgColor = data.roulette_color;
						let isRectBorder = false;
						if (!bgColor) {
							isRectBorder = true;
							if (["red", "blue", "green", "yellow"].indexOf(data.play_option) > -1) {
								bgColor = data.play_option;
							} else {
								bgColor = "black";
							}
						}
						html += "<div class='row'>" +
									"<div class='col-12'>" +
										"<div>" + showHistoryRouletteBall(data.play_option, bgColor, isRectBorder) + ": " + data.won + "</div>" +							
									"</div>" +
								"</div>";
					}
				});
			}
			html += "</div>";

			html += "<div class='row mt-1'>" +
						"<div id='history_balance_container_" + history.group + "' class='col-12' style='display: "
								+ (history.result === -1 ? "none" : "block") + ";'>" +
							"<div id='history_won_" + history.group + "'>Total Won: " + won.toFixed(2) + "</div>" +
							"<div class='mt-2' id='history_balance_" + history.group + "'>Win/Lose: <b style='color: "
								+ (total - won > 0 ? 'red': 'green') + ";'>" + (total - won).toFixed(2) + "</b></div>" +
							"<hr />" +
						"</div>" +
					"</div>";

			html += "<div class='row mt-2'>" +
						"<div class='col-12 ml-auto' style='text-align: right'>" +
							"<div>" +
								"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/refresh.png') }}' width='25' />" +
								"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/message.svg') }}' width='25' />" +
								"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/whatsapp.svg') }}' width='25' />" +
								"<img class='four-d-checkbox-badge' src='{{ asset('images/void.svg') }}' width='35' />" +
							"</div>" +
						"</div>" +
					"</div>" +
				"</div>" +
			"</div>";
			//
			document.getElementById("no_history").style.display = "none";
			if (isTop) {
				$(historyList).prepend(html);
			} else {
				$(historyList).append(html);
			}
		}
		
		function reBet() {
			rebet++;
			updateBetAmount();
			$("#rebet_count").text("x" + rebet);
			$("#rebet_count")[0].style.display = "inline-block";
		}
		
		function clearReBet() {
			rebet = 0;
			updateBetAmount();
			$("#rebet_count").hide();
		}
		
		function onCollapse() {
			const board = $('#board_container');
			const chip = $('#chip_container');
			const icon = $('#history_icon');
			isCollapse = icon.hasClass('icon-chevron-down');
			if (isCollapse) {
				board.animate({ height: "toggle" }, 500);
				chip.animate({ height: "toggle" }, 500);
				icon.removeClass('icon-chevron-down');
				icon.addClass('icon-chevron-up');
			} else {
				board.animate({ height: "toggle" }, 500);
				chip.animate({ height: "toggle" }, 500);
				icon.removeClass('icon-chevron-up');
				icon.addClass('icon-chevron-down');
				setTimeout(function() {
					loadRecordProcess(pendingRecords, "value");
				}, 500);
			}
		}
		
		function showHistoryRouletteBall(text, bg, isRectBorder) {
			// const i = document.createElement("i");
			//i.className = "mr-1 history-result p-1 background-" + bg;
			// i.innerText = text;
			// if (isRectBorder) {
				// i.style.borderRadius = "0.25rem";
			// }
			// return i.outerHTML;
			return text?.toString().replace("\n", "-");
		}
		
		function duplicateChipAudio() {
			const audio = document.createElement("audio");
			audio.volume = (Math.random() * 0.9) + 0.1;
			audio.src = chipAudio.getElementsByTagName("source")[0].src;
			audio.play().then(function() { audio.remove(); }).catch(function() {});
		}
		
		function disableDoubleTapZoom(evt) {
			let t2 = evt.timeStamp
			  , t1 = evt.target.dataset.lastTouch || t2
			  , dt = t2 - t1
			  , fingers = evt.touches.length;
			evt.target.dataset.lastTouch = t2;
			if (!dt || dt > 500 || fingers > 1) return; // not double-tap

			evt.preventDefault(); // double tap - prevent the zoom
			// also synthesize click events we just swallowed up
			evt.target.click();
		}
	</script>
	<script src="{{ asset('js/moment.min.js') }}" defer></script>
	<script src="{{ asset('js/roulette.js') }}?v=11" defer></script>
@endsection
@section('footer-js')
<script>
	const historyList = document.getElementById("history_list");
	const chipAudio = document.getElementById("chip_audio");	
	chipAudio.load();
	
	@if($interval == 0)
	// createRedPack();
	@else
	// setTimeout(() => createRedPack(), {{ $interval }} * 1000);
	@endif
	window.addEventListener('touchstart', disableDoubleTapZoom, { passive: false });
</script>
@endsection