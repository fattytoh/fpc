@extends('layouts.app')

@php($previous = true)

@section('button', "返回")

@section('title', "充值记录")

@section('theme', "black")

@section('css')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-8" style="margin-top: 50px;">
            <div class="card">
                <div class="card-body">
					<table id="table" class="table table-hover display responsive"></table>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/moment.min.js') }}" defer></script>
	<script src="{{ asset('js/datatables.min.js') }}" defer></script>
	<script type="text/javascript">
		let interval;
		const dtOpts = {
			listingUrl: "{{ Route('record.topup') }}",
			order: [[ 3, "desc" ]],
			columns: [{
				title: "{{ __('金额') }}",
				data: "amount"
			}, {
				title: "{{ __('渠道') }}",
				data: "method.name",
				defaultContent: "系统"
			}, {
				title: "{{ __('结果') }}",
				data: "status",
				defaultContent: "等待中"
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection