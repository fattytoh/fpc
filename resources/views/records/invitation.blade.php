@extends('layouts.app')

@php($previous = true)

@section('button', "返回")

@section('title', "邀请记录")

@section('theme', "black")

@section('css')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-8" style="margin-top: 50px;">
            <div class="card">
                <div class="card-body">
					<table id="table" class="table table-hover display responsive"></table>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/moment.min.js') }}" defer></script>
	<script src="{{ asset('js/datatables.min.js') }}" defer></script>
	<script type="text/javascript">
		let interval;
		const dtOpts = {
			listingUrl: "{{ Route('record.invitation') }}",
			order: [[ 3, "desc" ]],
			columns: [{
				title: "{{ __('邀请人') }}",
				data: "invited",
				render: function(data, dt, row) {
					return data ?
						("<b style='color: green;' title='" + data.created_at + "'>"
						+ data.name + "</b>") :
						("<i style='color: orange;'>" + row.mobile + "</i>");
				}
			}, {
				title: "{{ __('渠道') }}",
				data: "from",
				defaultContent: "系统"
			}, {
				title: "{{ __('结果') }}",
				data: "status",
				defaultContent: "等待中"
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					text: "{{ __('刷新') }}",
					action: function (e, dt) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection