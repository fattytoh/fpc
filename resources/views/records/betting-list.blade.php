@extends('layouts.app')

@php($previous = true)
@php($navbar = true)
@php($backRoute = route('room', ['id' => $room_id]))

@section('theme', "theme-red")

@section('title', "Record")

@section('css')
<style>
	@font-face{
		font-family: SFMonoLight;
		src: url({{  asset('fonts/SFMonoLight.otf') }});
	}
	
	.theme-red {
		background-color: red;
	}
</style>
<link href="{{ asset('css/bootadmin/feathericons.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-sm-12">
				</div>
			</div>
		</div>
        <div id="recordContainer" class="col-sm-12">
		</div>
    </div>
</div>
<!-- always on bottom -->
<div style="position: fixed; bottom: 0; left: 0; width: 100%;">
	<div class="loading-record alert alert-warning mb-0">
		<div class="text-center">Loading...</div>
	</div>
	<div class="empty-record alert alert-success mb-0" style="display: none;">
		<div class="text-center">No more 4d record.</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/record.js') }}" defer></script>
<script src="{{ asset('js/moment.min.js') }}" defer></script>
<script>
let offset = 0;
let loading = false;
function loadRecord() {
	if (loading) {
		return;
	}
	loading = true;
	$(".loading-record").show();
	$.ajax({
		url: "{{ route('record.betting.list', ['id' => $room_id]) }}",
		type: "POST",
		data: {
			offset: offset
		},
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	}).done(function(res) {
		// render
		$(".loading-record").hide();
		if (res.data.length === 0) {
			$(".empty-record").show();
			setTimeout(() => {
				$(".empty-record").hide();
				loading = false;
			}, 1000);
			return;
		}
		$(".empty-record").hide();
		offset += 10;
		
		const grouped = [];
		
		res.data.forEach(data => {
			const group = data.group;
			if (!grouped[group]) {
				grouped[group] = [];
			}
			const key = data.play_option + ":" + data.play_group + ":" + ":" + (data.group2 || 0);
			if (!grouped[group][key]) {
				grouped[group][key] = [];
			}
			grouped[group][key].push(data);
		});
		Object.keys(grouped).forEach(group => {
			const grouped2 = grouped[group];
			const keys2 = Object.keys(grouped2);
			const first = grouped2[keys2[0]][0];
			const cDate = moment(first.created_at);
			
			let html = "<div class='card mb-3'>" +
				"<div class='card-body'>" +
					"<div class='row mb-2'>" +
						"<div class='col-7 col-sm-10'>" +
							"<div class='text-secondary'>" + cDate.format("Do MMM • HH:mm") + "</div>" +
						"</div>" +
						"<div class='col-5 col-sm-2'  style='text-align: right'>" +
							(first.result === -1 ?
							"<div class='btn btn-warning p-0 px-1' style='font-size: 12px'><b>Committed</b></div>" :
							"<div class='btn btn-primary p-0 px-1' style='font-size: 12px'><b>Confirmed</b></div>") +
						"</div>" +
					"</div>" +
					"<div style='font-family: SFMonoLight;'>" +
						"<div class='row mb-2'>" +
							"<div class='col-12'>" +
								"<p>Ref #" + group + "</p>" +
							"</div>" +
						"</div>";
			let total = 0;
			keys2.forEach(option => {
				const types = grouped2[option];
				let icons = "";
				let version = "";
				types.forEach(data => {
					icons += "<img class='four-d-checkbox-badge mr-1' src='{{ asset('images/4d') }}/" + data.play_name + ".png' width='20' />";
					total += data.amount;
					version = data.betting?.version;
				});
				const options = option.split(":");
				const nVersion = parseInt(version);
				html += "<div class='row mb-2'>" +
							"<div class='col-12'>" +
								"<div>" + (nVersion > -1 ? "Draw No: " + version : "Days: " + -nVersion) + "</div>" +
								"<div>" + icons + "</div>" +
								"<p>" + options[0] + "=" + options[1] + "</p>" +							
							"</div>" +
						"</div>";
			});
			
			html += "<div class='row my-5'>" +
						"<div class='col-12'>" +
							"<p>Total: " + total.toFixed(2) + "</p>" +
						"</div>" +
					"</div>" +
				"</div>" +
				"<div class='row'>" +
					"<div class='col-12 ml-auto' style='text-align: right'>" +
						"<div>" +
							"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/refresh.png') }}' width='25' />" +
							"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/message.svg') }}' width='25' />" +
							"<img class='four-d-checkbox-badge mr-3' src='{{ asset('images/whatsapp.svg') }}' width='25' />" +
							"<img class='four-d-checkbox-badge' src='{{ asset('images/void.svg') }}' width='35' /></div>" +
						"</div>" +
					"</div>" +
				"</div>" +
			"</div>";
			
			const record = $(html);
			$("#recordContainer").append(record);
		});
		loading = false;
	}).fail(function(err) {
		loading = false;
		Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
	});
}
</script>
@endsection