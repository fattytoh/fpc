<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
	'register' => false,
	'reset' => false
]);

// API with session
Route::get('launch', [App\Http\Controllers\APIController::class, 'launch'])->name('launch');
//

Route::get('redirect/{gameId}/{accountCode}', [App\Http\Controllers\APIController::class, 'redirect'])->name('redirect');

Route::get('singleuser/balance/{accountCode}', [App\Http\Controllers\APIController::class, 'getSingleCredit']);

Route::post('commit', [App\Http\Controllers\User\HomeController::class, 'commit'])->name('commit');
	
Route::get('/', [App\Http\Controllers\PublicController::class, 'index'])->name('landing');

Route::get('lobby', [App\Http\Controllers\User\HomeController::class, 'lobby'])->name('lobby');

Route::post('status', [App\Http\Controllers\User\HomeController::class, 'get'])->name('status');

Route::get('result/{id}', [App\Http\Controllers\User\BettingResultController::class, 'index'])->name('result');

Route::post('result/{id}', [App\Http\Controllers\User\BettingResultController::class, 'result']);

// Route::get('topup', [App\Http\Controllers\User\TopupController::class, 'index'])->name('topup');

// Route::post('topup', [App\Http\Controllers\User\TopupController::class, 'topup']);

Route::group(['prefix' => 'record'], function() {
	Route::get('betting', [App\Http\Controllers\User\BettingRecordController::class, 'index'])->name('record.betting');

	Route::post('betting', [App\Http\Controllers\User\BettingRecordController::class, 'listing']);

	Route::get('betting/list/{id}', [App\Http\Controllers\User\BettingRecordController::class, 'index4d'])->name('record.betting.list');

	Route::post('betting/list/{id}', [App\Http\Controllers\User\BettingRecordController::class, 'listing4d']);

	Route::get('invitation', [App\Http\Controllers\User\InvitationRecordController::class, 'index'])->name('record.invitation');

	Route::post('invitation', [App\Http\Controllers\User\InvitationRecordController::class, 'listing']);

	Route::get('topup', [App\Http\Controllers\User\TopupRecordController::class, 'index'])->name('record.topup');

	Route::post('topup', [App\Http\Controllers\User\TopupRecordController::class, 'listing']);

	Route::get('withdraw', [App\Http\Controllers\User\WithdrawRecordController::class, 'index'])->name('record.withdraw');

	Route::post('withdraw', [App\Http\Controllers\User\WithdrawRecordController::class, 'listing']);
});

Route::group(['prefix' => 'room'], function() {
	Route::get('{id}', [App\Http\Controllers\User\BettingController::class, 'index'])->name('room');

	Route::post('{id}', [App\Http\Controllers\User\BettingController::class, 'status']);

	Route::post('{id}/bet', [App\Http\Controllers\User\BettingController::class, 'bet'])->name('room.bet');

	Route::post('{id}/bet/4d', [App\Http\Controllers\User\BettingController::class, 'bet4d'])->name('room.bet.4d');

	Route::post('{id}/record', [App\Http\Controllers\User\BettingController::class, 'record'])->name('room.record');

	Route::post('{id}/clear', [App\Http\Controllers\User\BettingController::class, 'clear'])->name('room.clear');

	Route::post('{id}/redpack', [App\Http\Controllers\User\BettingController::class, 'redpack'])->name('room.redpack');

	Route::post('{id}/rebet', [App\Http\Controllers\User\BettingController::class, 'rebet'])->name('room.rebet');

	Route::post('{id}/rebet/last', [App\Http\Controllers\User\BettingController::class, 'rebetLast'])->name('room.rebetLast');
});

Route::post('withdraw', [App\Http\Controllers\User\HomeController::class, 'withdraw'])->name('withdraw');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {	
	Route::get('login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'showLoginForm'])->name('login');
	Route::post('login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'login']);
	Route::post('logout', [App\Http\Controllers\Admin\Auth\LoginController::class, 'logout'])->name('logout');
	
	Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('dashboard');
	
	Route::group(['middleware' => 'role_control'], function() {
		Route::group(['prefix' => 'banner', 'as' => 'banner'], function() {
			Route::get('/', [App\Http\Controllers\Admin\BannerController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\BannerController::class, 'listing'])->name(".listing");;
		
			Route::post('create', [App\Http\Controllers\Admin\BannerController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\BannerController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\BannerController::class, 'destroy'])->name(".delete");
		
			Route::post('upload', [App\Http\Controllers\Admin\BannerController::class, 'upload'])->name(".upload");
		});
		
		Route::group(['prefix' => 'betting', 'as' => 'betting'], function() {
			Route::get('/', [App\Http\Controllers\Admin\BettingController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\BettingController::class, 'listing'])->name(".listing");;
		
			Route::post('create', [App\Http\Controllers\Admin\BettingController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\BettingController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\BettingController::class, 'destroy'])->name(".delete");
		
			Route::post('random', [App\Http\Controllers\Admin\BettingController::class, 'random'])->name(".random");
		
			Route::post('deliver', [App\Http\Controllers\Admin\BettingController::class, 'deliver'])->name(".deliver");
		
			Route::group(['prefix' => 'record', 'as' => '.record'], function() {
				Route::get('/', [App\Http\Controllers\Admin\BettingRecordController::class, 'index']);

				Route::post('/', [App\Http\Controllers\Admin\BettingRecordController::class, 'listing'])->name(".listing");
				
				Route::group(['prefix' => '4d', 'as' => '.4d'], function() {
					Route::get('/', [App\Http\Controllers\Admin\BettingRecordController::class, 'index4d']);

					Route::post('/', [App\Http\Controllers\Admin\BettingRecordController::class, 'listing4d'])->name(".listing");
				});
			});
		});

		Route::group(['prefix' => 'invitation', 'as' => 'invitation'], function() {
			Route::group(['prefix' => 'record', 'as' => '.record'], function() {
				Route::get('/', [App\Http\Controllers\Admin\InvitationRecordController::class, 'index']);
			
				Route::post('/', [App\Http\Controllers\Admin\InvitationRecordController::class, 'listing'])->name(".listing");
			});
		});
		
		Route::group(['prefix' => 'role', 'as' => 'role'], function() {
			Route::get('/', [App\Http\Controllers\Admin\RoleController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\RoleController::class, 'listing'])->name(".listing");;
		
			Route::post('create', [App\Http\Controllers\Admin\RoleController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\RoleController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\RoleController::class, 'destroy'])->name(".delete");
		
			Route::group(['prefix' => 'access', 'as' => '.access'], function() {
				Route::get('/', [App\Http\Controllers\Admin\RoleAccessController::class, 'index']);
			
				Route::post('/', [App\Http\Controllers\Admin\RoleAccessController::class, 'listing'])->name(".listing");;
			
				Route::post('create', [App\Http\Controllers\Admin\RoleAccessController::class, 'create'])->name(".create");
			
				Route::post('update', [App\Http\Controllers\Admin\RoleAccessController::class, 'update'])->name(".update");
			
				Route::delete('delete', [App\Http\Controllers\Admin\RoleAccessController::class, 'destroy'])->name(".delete");
			});
		});
		
		Route::group(['prefix' => 'room', 'as' => 'room'], function() {
			Route::get('/', [App\Http\Controllers\Admin\RoomController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\RoomController::class, 'listing'])->name(".listing");
		
			Route::post('create', [App\Http\Controllers\Admin\RoomController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\RoomController::class, 'update'])->name(".update");
		
			Route::post('upload', [App\Http\Controllers\Admin\RoomController::class, 'upload'])->name(".upload");
		
			Route::delete('delete', [App\Http\Controllers\Admin\RoomController::class, 'destroy'])->name(".delete");
		
			Route::group(['prefix' => '{id?}', 'as' => '.rate'], function() {
				Route::get('/', [App\Http\Controllers\Admin\RoomRateController::class, 'index']);
			
				Route::post('/', [App\Http\Controllers\Admin\RoomRateController::class, 'listing'])->name(".listing");
			
				Route::post('create', [App\Http\Controllers\Admin\RoomRateController::class, 'create'])->name(".create");
			
				Route::post('update', [App\Http\Controllers\Admin\RoomRateController::class, 'update'])->name(".update");
			
				Route::delete('delete', [App\Http\Controllers\Admin\RoomRateController::class, 'destroy'])->name(".delete");
			});
		});

		Route::group(['prefix' => 'setting', 'as' => 'setting'], function() {			
			Route::get('/', [App\Http\Controllers\Admin\SettingController::class, 'index']);
			
			Route::post('update', [App\Http\Controllers\Admin\SettingController::class, 'update'])->name('.update');
		});
		
		Route::group(['prefix' => 'staff', 'as' => 'staff'], function() {
			Route::get('/', [App\Http\Controllers\Admin\StaffController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\StaffController::class, 'listing'])->name(".listing");;
		
			Route::post('create', [App\Http\Controllers\Admin\StaffController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\StaffController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\StaffController::class, 'destroy'])->name(".delete");
		
			Route::post('api', [App\Http\Controllers\Admin\StaffController::class, 'api'])->name(".api");
		});

		Route::group(['prefix' => 'topup', 'as' => 'topup'], function() {
			Route::group(['prefix' => 'record', 'as' => '.record'], function() {
				Route::get('/', [App\Http\Controllers\Admin\TopupRecordController::class, 'index']);
			
				Route::post('/', [App\Http\Controllers\Admin\TopupRecordController::class, 'listing'])->name(".listing");
			});
		
			Route::post('create', [App\Http\Controllers\Admin\TopupRecordController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\TopupRecordController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\TopupRecordController::class, 'destroy'])->name(".delete");
		});

		Route::group(['prefix' => 'user', 'as' => 'user'], function() {
			Route::get('/', [App\Http\Controllers\Admin\UserController::class, 'index']);
		
			Route::post('/', [App\Http\Controllers\Admin\UserController::class, 'listing'])->name(".listing");;
		
			Route::post('create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name(".create");
		
			Route::post('update', [App\Http\Controllers\Admin\UserController::class, 'update'])->name(".update");
		
			Route::delete('delete', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name(".delete");
		});

		Route::group(['prefix' => 'withdraw', 'as' => 'withdraw'], function() {
			Route::group(['prefix' => 'record', 'as' => '.record'], function() {
				Route::get('/', [App\Http\Controllers\Admin\WithdrawRecordController::class, 'index']);
			
				Route::post('/', [App\Http\Controllers\Admin\WithdrawRecordController::class, 'listing'])->name(".listing");
		
				Route::post('update', [App\Http\Controllers\Admin\WithdrawRecordController::class, 'update'])->name(".update");
			
				Route::delete('delete', [App\Http\Controllers\Admin\WithdrawRecordController::class, 'destroy'])->name(".delete");
			});
		});
	});
});