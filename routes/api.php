<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('token', [App\Http\Controllers\API\APIController::class, 'token']);

Route::post('rooms', [App\Http\Controllers\API\APIController::class, 'rooms']);

Route::post('user/create', [App\Http\Controllers\API\APIController::class, 'createUser']);

Route::post('user/balance', [App\Http\Controllers\API\APIController::class, 'getCredit']);

Route::post('user/adjust', [App\Http\Controllers\API\APIController::class, 'adjustPlayerBalance']);

Route::post('user/adjust/void', [App\Http\Controllers\API\APIController::class, 'voidAdjust']);

Route::post('game/logs', [App\Http\Controllers\API\APIController::class, 'gameLogs']);

Route::post('launch', [App\Http\Controllers\API\APIController::class, 'launch']);