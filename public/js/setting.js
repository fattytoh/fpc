$(document).on('click', '.setting-click', function() {
	updateSetting(this);
});
	
$(document).on('change', '.setting-change', function() {
	updateSetting(this);
});

$(document).on('click', '.setting-button', function() {
	var access = $("#access").val();
	
	if(access) {
		$("#access").val("");
		updateSetting(this, access);
	} else {
		$("#access").addClass("is-invalid");
		$("#access").focus();
	}
});

function updateSetting(element, access = null) {
	$("#access").removeClass("is-invalid");
	
	var item = $(element);
	
	if(!item.hasClass('disabled')) {
		var data = item.data();
		
		disableControls(item, data.disabled, data.parent, data.key);
		
		$.ajax({
			url: settingUrl,
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {
				key: data.key,
				value: item.val(),
				access: access
			}
		}).done(function(res) {
			enableControls(item, data.disabled, data.parent, data.key);
			
			if(res) {
				swal.fire(data.name + " success", res, "success");
			}
		}).fail(function(err) {
			enableControls(item, data.disabled, data.parent, data.key);			
			// display error
			swal.fire("Update on " + data.name + " failed", err.responseJSON ? err.responseJSON.message : (err.statusText + "."), "error");
		});
	}
}

function disableControls(item, type, parent, key) {
	if(type) {
		var matchs = $(parent).find('[' + type + '=' + key + ']');
		
		matchs.each(function(index, match) {
			var mItem = $(match);
			
			mItem.addClass("disabled");
			mItem.attr('disabled', true);
		});
	} else {
		item.addClass("disabled");
		item.attr('disabled', true);
	}
}

function enableControls(item, type, parent, key) {
	if(type) {
		var matchs = $(parent).find('[' + type + '=' + key + ']');
		
		matchs.each(function(index, match) {
			var mItem = $(match);
			
			mItem.removeClass("disabled");
			mItem.removeAttr('disabled');
		});
	} else {
		item.removeClass("disabled");
		item.removeAttr('disabled');
	}
}