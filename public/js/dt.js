// moment plugin on DataTable
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "moment", "datatables.net"], factory);
    } else {
        factory(jQuery, moment);
    }
}(function ($, moment) {
 
function strip (d) {
    if ( typeof d === 'string' ) {
        // Strip HTML tags and newline characters if possible
        d = d.replace(/(<.*?>)|(\r?\n|\r)/g, '');
 
        // Strip out surrounding white space
        d = d.trim();
    }
 
    return d;
}
 
$.fn.DataTable.moment = function ( format, locale, reverseEmpties ) {
    var types = $.fn.dataTable.ext.type;
 
    // Add type detection
    types.detect.unshift( function ( d ) {
        d = strip(d);
 
        // Null and empty values are acceptable
        if ( d === '' || d === null ) {
            return 'moment-'+format;
        }
 
        return moment( d, format, locale, true ).isValid() ?
            'moment-'+format :
            null;
    } );
 
    // Add sorting method - use an integer for the sorting
    types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
        d = strip(d);
         
        return !moment(d, format, locale, true).isValid() ?
            (reverseEmpties ? -Infinity : Infinity) :
            parseInt( moment( d, format, locale, true ).format( 'x' ), 10 );
    };
};
}));
// breakline plugin on Editor
(function ($, DataTable) {
 if (!DataTable.ext.editorFields) {
    DataTable.ext.editorFields = {};
}
DataTable.ext.editorFields.breakline = {
    create: function ( conf ) {
        var that = this;
 
        conf._enabled = true;
        conf._input = $('<hr/>'); 
        return conf._input;
    },
 
    get: function ( conf ) {
        return null;
    },
 
    set: function ( conf, val ) {
    },
 
    enable: function ( conf ) {
        conf._enabled = true;
    },
 
    disable: function ( conf ) {
        conf._enabled = false;
    }
};
})(jQuery, jQuery.fn.dataTable);
// logic
let options = {
	tableName: "#table"
};
try {
	Object.keys(dtOpts).forEach(function(key) {
		options[key] = dtOpts[key];
	});
} catch {
}
function InitEditor() {
	return new $.fn.DataTable.Editor({
		ajax: {		
			create: {
				url: options.createUrl,
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},	
			edit: {
				url: options.updateUrl,
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			upload: {
				url: options.uploadUrl,
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			},
			remove: {
				url: options.deleteUrl,
				type: "DELETE",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
			}
		},
		table: options.tableName,
		idSrc:  'id',
		fields: options.fields
	});
}

function InitDT(editor) {	
    $.fn.DataTable.moment('M/D/YYYY, h:mm:ss A');
	return $(options.tableName).DataTable({
        processing: options.processing || false,
		serverSide: options.serverSide || false,
		order: options.order,
		dom: options.dom || "Bfrtip",		
		ajax: options.listingUrl ? {
			url: options.listingUrl,
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		} : undefined,
		rowId: "id",
        columns: options.columns,
		select: true,
        buttons: options.buttons ? options.buttons(editor) : []
	});
}
let gEditor = null;
if(options.createUrl || options.updateUrl || options.deleteUrl) {
	gEditor = InitEditor();
	if(options.hideOnCreate) {
		gEditor.on('initCreate', function() {
			gEditor.show(); //Shows all fields
			gEditor.hide(options.hideOnCreate);
		});
	}
	if(options.hideOnEdit) {
		gEditor.on('initEdit', function() {
			gEditor.show(); //Shows all fields
			gEditor.hide(options.hideOnEdit);
		});
	}
	if(options.postCreate) {
		gEditor.on('postCreate', (e, json, data, id) =>
			options.postCreate(gEditor, e, json, data, id));
	}
	if(options.postEdit) {
		gEditor.on('postEdit', (e, json, data, id) =>
			options.postEdit(gEditor, e, json, data, id));
	}
	if(options.postRemove) {
		gEditor.on('postRemove', (e, json, data, id) =>
			options.postRemove(gEditor, e, json, data, id));
	}
}
let dTable = InitDT(gEditor);

if(options.onDraw) {
	dTable.on('draw', options.onDraw);
}