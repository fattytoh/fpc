<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Betting extends Model
{
    use HasFactory;

	protected $fillable = ["room_id", "ended_at", "version", 'standalone_user', 'group', 'date'];
	protected $casts = [
		"ended_at" => "datetime"
	];
	
	const FPC_ORDER = ['', 'crab', 'fish', 'hulu', 'tiger', 'chicken', 'prawn'];
	const FOUR_D = [//'F' => 'fortune-star', 
	'M' => 'magnum', 'P' => 'damacai', 'T' => 'toto', 'S' => 'singapore', 'B' => 'lotto88', 'K' => 'stc', 'W' => 'cashsweep'];
	const DICE_BIG = 11;
	const DICE_MIN = 1;
	const DICE_MAX = 6;
	const DICE_COUNT = 3;
	const DICE_NAME = "ball";
	const DICE_RATE = ["大" => 1.97, "小" => 1.97, "单" => 1.97, "双" => 1.97
		, "3" => 191.16, "4" => 63.72, "5" => 31.86, "6" => 19.11
		, "7" => 12.74, "8" => 9.1, "9" => 7.64, "10" => 7.08
		, "11" => 7.08, "12" => 7.64, "13" => 9.1, "14" => 12.74
		, "15" => 19.11, "16" => 31.86, "17" => 63.72, "18" => 191.16];
	const DICE_MIN_TOTAL = self::DICE_COUNT * self::DICE_MIN;
	const DICE_MAX_TOTAL = self::DICE_COUNT * self::DICE_MAX;
	
	public function room() {
		return $this->belongsTo(Room::class);
	}
	
	public function records() {
		return $this->hasMany(BettingRecord::class);
	}
	
	public function results() {
		return $this->hasMany(BettingResult::class);
	}

	public function users() {
		return $this->hasManyThrough(User::class, BettingRecord::class
			, "betting_id", "id", "id", "user_id");
	}

	public function uniqueUsers() {
		return $this->hasManyThrough(User::class, BettingRecord::class
			, "betting_id", "id", "id", "user_id")->select("users.id", "betting_records.betting_id")
			->groupBy("users.id", "betting_records.betting_id");
	}

	public function getTotalAttribute() {
		if($this->results->count() > 0) {
			return $this->results->sum("value");
		} else {
			return null;
		}
	}
	
	private function startLoopPossibilities($options, $count, $noDuplicate = false) {
		return $this->loopPossibilities([], $options, 1, $count, $noDuplicate);
	}
	
	private function loopPossibilities($values, $options, $currIndex, $totalIndex, $noDuplicate) {
		$array = [];
		foreach ($options as $i) {
			$newValues = $values;
			array_push($newValues, $i);
			if ($currIndex < $totalIndex) {
				$array = array_merge($array, $this->loopPossibilities($newValues
					, $options, $currIndex + 1, $totalIndex, $noDuplicate));
			} else {
				array_push($array, $newValues);
			}
		}
		if ($totalIndex == $currIndex && $noDuplicate) {
			foreach ($array as $index => $item) {
				if (count(array_unique($item)) == 1) {
					array_splice($array, $index, 1);
				}
			}
		}
		return $array;
	}

	public function endBetting() {
		if(!$this->reward || $this->total === null) {
			$bettingSetting = Setting::where('key', 'betting')->first();
			$bettingType = $bettingSetting ? $bettingSetting->value : "0";
			$game = $this->room ? $this->room->game : new Game([
				"count" => 1,
				"min" => 0,
				"max" => 1
			]);
			if($this->records->count() > 0 && $bettingType != "0") {
				$playTotals = [];
				// $totalOut = 0;
				// $totalIn = 0;
				// $avoids = [];
				$rates = [];
				foreach ($this->room->rates()->with('group')->get() as $roomRate) {
					if (!$roomRate->group) {
						continue;
					}
					if (!isset($rates[$roomRate->group->group])) {
						$rates[$roomRate->group->group] = [];
					}
					$rates[$roomRate->group->group][$roomRate->matched] = $roomRate->rate;
				}
				$possibleResults = $this->startLoopPossibilities(range($game->min, $game->max), $game->count);
				foreach($this->records as $item) {
					$options = array_unique(explode("-", $item->play_option));
					if($bettingType == "2") {
						$possibleOptions = $this->startLoopPossibilities($options, $game->count, true);
						foreach ($possibleOptions as $pOption) {
							if($index = array_search($pOption, $possibleResults)) {
								array_splice($possibleResults, $index, 1);
							}
						}
					}
				}
				if ($bettingType == "1") {
					// do something?
				}
				if (($possibleCount = count($possibleResults)) > 0) {
					$resultIndex = rand(0, $possibleCount - 1);
					//dd($resultIndex);
					foreach ($possibleResults[$resultIndex] as $index => $resultValue) {
						$result = new BettingResult([
							'order' => $index,
							'value' => $resultValue
						]);
						$this->results()->save($result);
					}
					return $this;
				}
					// if (!$roomRate = $rates[$item->play_group]) {
						// something wrong? not record for this room?
						// dd($item->play_group);
						// continue;
					// }
					// $possibilities = [];
					// if (count($options) == 1) {
						// array_push($possibilities, implode('-', array_fill(0, $roomRate->matched, $options[0])));
					// } else if (count($options) < $roomRate->matched) {
						// array_push($possibilities, implode('-', $options);
					// } else {
						// [1, 2]
						// 3
						// [1, 2]
						// $this->loopPossibilities([], 0, count($options), 0, $roomRate->matched);
					// }
					// $key = $options[0];
					// $amount = $roomRate->rate * $item->amount;
					// if(isset($playTotals[$key])) {
						// $playTotals[$key] += $amount;
					// } else {
						// $playTotals[$key] = $amount;
					// }
					// $totalIn += $item->amount;
					// $totalOut += $amount;
				// if($bettingType == "3") {
					// $betting1Setting = Setting::where('key', 'betting.1.min')->first();
					// $betting1Value = $betting1Setting ? (int)$betting1Setting->value : 0;
					// $betting2Setting = Setting::where('key', 'betting.2.min')->first();
					// $betting2Value = $betting2Setting ? (int)$betting2Setting->value : 0;
					
					// if($totalOut >= $betting2Value && count($playTotals) < $ratesCount) {
						// foreach(array_keys($playTotals) as $key) {
							// if($isBigSmall = in_array($key, ["大", "小"]) || $isOddEven = in_array($key, ["单", "双"])) {
								// if($isBigSmall) {
									// $numbers = $this->generateBigSmallPossibilities($key == "大");
								// } else {
									// $numbers = $this->generateOddEvenPossibilities($key == "单");
								// }					
								// foreach($numbers as $key) {
									// array_push($avoids, $key);
								// }
							// } else {
								// array_push($avoids, $key);
							// }
						// }
					// } else if($totalOut >= $betting1Value) {
						// $bettingRateSetting = Setting::where('key', 'betting.1.rate')->first();
						// $bettingRate = ($bettingRateSetting ? (float)$bettingRateSetting->value : 49) / 50;
						// $count = count($playTotals);
						// for($i = 0; $i < $count && $totalIn * $bettingRate > $totalOut; $i++) {
							// $key = array_keys($playTotals)[rand(0, count($playTotals) - 1)];
							// if($isBigSmall = in_array($key, ["大", "小"]) || $isOddEven = in_array($key, ["单", "双"])) {
								// if($isBigSmall) {
									// $numbers = $this->generateBigSmallPossibilities($key == "大");
								// } else {
									// $numbers = $this->generateOddEvenPossibilities($key == "单");
								// }
								// $totalOut -= $playTotals[$key];						
								// foreach($numbers as $key) {
									// array_push($avoids, $key);
									// if(isset($playTotals[$key])) {
										// $totalOut -= $playTotals[$key];
										// unset($playTotals[$key]);
									// }
								// }
							// } else {
								// $totalOut -= $playTotals[$key];
								// array_push($avoids, $key);
								// unset($playTotals[$key]);
							// }
						// }
					// }
				// } else if($bettingType == "2" && count($playTotals) < $ratesCount) {
					// foreach(array_keys($playTotals) as $key) {
						// if($isBigSmall = in_array($key, ["大", "小"]) || $isOddEven = in_array($key, ["单", "双"])) {
							// if($isBigSmall) {
								// $numbers = $this->generateBigSmallPossibilities($key == "大");
							// } else {
								// $numbers = $this->generateOddEvenPossibilities($key == "单");
							// }					
							// foreach($numbers as $key) {
								// array_push($avoids, $key);
							// }
						// } else {
							// array_push($avoids, $key);
						// }
					// }					
				// } else if($bettingType == "1") {
					// $bettingRateSetting = Setting::where('key', 'betting.1.rate')->first();
					// $bettingRate = ($bettingRateSetting ? (float)$bettingRateSetting->value : 49) / 50;
					// $count = count($playTotals);
					// for($i = 0; $i < $count && $totalIn * $bettingRate < $totalOut; $i++) {
						// $key = array_keys($playTotals)[rand(0, count($playTotals) - 1)];
						// if($isBigSmall = in_array($key, ["大", "小"]) || $isOddEven = in_array($key, ["单", "双"])) {
							// if($isBigSmall) {
								// $numbers = $this->generateBigSmallPossibilities($key == "大");
							// } else {
								// $numbers = $this->generateOddEvenPossibilities($key == "单");
							// }
							// $totalOut -= $playTotals[$key];						
							// foreach($numbers as $key) {
								// array_push($avoids, $key);
								// if(isset($playTotals[$key])) {
									// $totalOut -= $playTotals[$key];
									// unset($playTotals[$key]);
								// }
							// }
						// } else {
							// $totalOut -= $playTotals[$key];
							// array_push($avoids, $key);
							// unset($playTotals[$key]);
						// }
					// }
				// }
				// if($ratesCount > 0 && ($aRates = $this->room->rates()->whereNotIn('name', $avoids)->get())->count() > 0) {
					// $rate = $aRates->get(rand(0, $aRates->count() - 1));
					// $possibilities = $this->generateNumericPossibilities($rate->name);
										
					// if(count($possibilities) > 0) {
						// $possibility = $possibilities[rand(0, count($possibilities) - 1)];
						// for($i = 1; $i <= self::DICE_COUNT; $i++) {
							// $this[self::DICE_NAME . $i] = $possibility[$i - 1];
						// }
					// }
				// }				
				// if($this->total === null) {
					// for($i = 1; $i <= self::DICE_COUNT; $i++) {
						// $this[self::DICE_NAME . $i] = rand(self::DICE_MIN, self::DICE_MAX);
					// }
				// }
			}
			// fallback
			for($i = 0; $i < ($game ? $game->count : 1); $i++) {
				$result = new BettingResult([
					'order' => $i,
					'value' => $game ? rand($game->min, $game->max) : rand(0, 1)
				]);
				$this->results()->save($result);
			}
		}
		return $this;
	}

	public function deliverReward(bool $updateEndDate = false) {
		if(!$this->reward) {
			foreach($this->records as $record) {
				if($record->won === null) {
					$record->won = $record->result > 0 ? $record->amount
						* ($record->result + 1) : 0;
					$record->save();
					// update reward
					$record->user->won += $record->won;
					$record->user->save();
				}
			}
			$this->reward = true;
			if($updateEndDate) {
				$this->ended_at = Carbon::now();
			}
			return $this->save();
		} else {
			return false;
		}
	}
	
	public function generateNumericPossibilities($total) {
		$possibilities = [];
		for($i = self::DICE_MIN; $i <= self::DICE_MAX; $i++) {
			for($j = self::DICE_MIN; $j <= self::DICE_MAX; $j++) {
				for($k = self::DICE_MIN; $k <= self::DICE_MAX; $k++) {
					if($i + $j + $k == $total) {
						array_push($possibilities, [$i, $j, $k]);
					}
				}
			}
		}
		return $possibilities;
	}
	
	public function generateBigSmallPossibilities($isBig) {
		$possibilities = [];
		if($isBig) {
			for($i = self::DICE_BIG; $i <= self::DICE_MAX_TOTAL; $i++) {
				array_push($possibilities, $i);
			}
		} else {
			for($i = self::DICE_BIG - 1; $i >= self::DICE_MIN_TOTAL; $i--) {
				array_push($possibilities, $i);
			}
		}
		return $possibilities;
	}
	
	public function generateOddEvenPossibilities($isEven) {
		$possibilities = [];
		for($i = self::DICE_MIN_TOTAL; $i <= self::DICE_MAX_TOTAL; $i++) {
			if($i % 2 == ($isEven ? 1 : 0)) {
				array_push($possibilities, $i);
			}
		}
		return $possibilities;
	}
}
