<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomRate extends Model
{
    use HasFactory;
	
	protected $fillable = ["name", 'matched', "rate"];
	
	protected $casts = [
		"rate" => "float"
	];
	
	public function group() {
		return $this->belongsTo(RoomRateGroup::class, "group_id");
	}
}
