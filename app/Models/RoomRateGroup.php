<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RoomRateGroup extends Model
{
    use HasFactory;
	
	protected $fillable = ["name", "group", "updated_by"];
	
	public function rates() {
		return $this->hasMany(RoomRate::class, 'group_id');
	}
	
	public function updatedBy() {
		return $this->belongsTo(Admin::class, 'updated_by');
	}
}
