<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    use HasFactory;
		
    protected $fillable = [
        'name', 'modules', 'available'
    ];
	
	protected $casts = [
		'available' => "boolean"
	];
	
	public function roles() {
		return $this->hasManyThrough(AdminRole::class, AdminRoleAccess::class
			, "access_id", "id", "id", "role_id");
	}
}
