<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationRecord extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'inviter_id', 'invited_id', 'from', 'status', 'mobile'
	];
}
