<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RoomGroup extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name',
        'group'
    ];
	
	public function rooms() {
		return $this->hasMany(Room::class, 'group', 'group');
	}
}
