<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRoleAccess extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'role_id', 'access_id'
    ];
	
	public $timestamps = false;
	
	public function role() {
		return $this->belongsTo(AdminRole::class);
	}
	
	public function access() {
		return $this->belongsTo(RoleAccess::class);
	}
}
