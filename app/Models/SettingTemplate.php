<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingTemplate extends Model
{
	protected $fillable = [
		'name', 'key', 'type'
	];
	
	public $timestamps = false;
	
    public function details() {
		return $this->hasMany(SettingTemplateDetail::class, 'template_id');
	}
}
