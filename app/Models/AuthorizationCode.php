<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AuthorizationCode extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'code', 'user_id', 'admin_id'
    ];
	
	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function admin() {
		return $this->belongsTo(Admin::class);
	}
}
