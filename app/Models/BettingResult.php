<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BettingResult extends Model
{
    use HasFactory;
	protected $fillable = ["order", "value", "betting_id", "api_prize", "api_ref"];
	public $timestamps = false;
	protected $appends = ["roulette_color"];

	public function betting() {
		return $this->belongsTo(Betting::class);
	}
	
	public function getRouletteColorAttribute() {
		if (is_numeric($this->value)) {
			$value = str_pad($this->value, 2, "0", STR_PAD_LEFT);
			$i = $value[0];
			$j = $value[1];
			return $i < 5 && 10 - $j - $i - 1 > 0 && $j - $i + 1 > 0 ? 'yellow' : (
				$j > 4 && $j - $i > 0 ? 'green': (
				$i > 4 && 10 - $j - $i - 1 < 0 && $j - $i - 1 < 0 ? 'blue': 'red'));
		} else {
			return "";
		}
	}
}
