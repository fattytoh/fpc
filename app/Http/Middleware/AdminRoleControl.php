<?php

namespace App\Http\Middleware;

use App\RouteGuard;
use Closure;
use Illuminate\Http\Request;

class AdminRoleControl
{
	private function restrict($result, Request $request, Closure $next) {		
		if ($request->expectsJson()) {
			return response()->json(["error" => $result === null ? "尚未安排职位。" : "工作范围外。"]);
		} else {
			return redirect('admin')->withErrors(['message' => $result === null ? "尚未安排职位。" : '工作范围外。']);
		}
	}
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		if($result = RouteGuard::available($request->path())) {
			return $next($request);
		} else {
			return $this->restrict($result, $request, $next);
		}
		
		return $next($request);
    }
}
