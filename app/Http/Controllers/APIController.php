<?php

namespace App\Http\Controllers;

use App\Models\AuthorizationCode;
use App\Models\User;
use Auth;
use Request;

class APIController extends Controller
{	
	public function redirect($gameId, $accountCode) {
		$user = User::where('username', $accountCode)->first();
		if (!$user) {
			$user = User::create([
				'username' => $accountCode,
				'name' => $accountCode,
				'password' => bcrypt(""),
				'invitation_code' => "",
				'real_name' => "",
				'card_number' => "",
				'withdraw_pin' => "",
				'from_redirect' => true
			]);
			$amount = Request::input("amount");
			if ($amount) {
				$user->topupRecords()->create([
					"amount" => $amount,
					"status" => "充值完成",
					"method_id" => 3,
					"remark" => ""
				]);
			}
		} else if (!$user->from_redirect) {
			abort(401, "This user is not supported with API login");
		}
		$loggedIn = Auth::guard('web')->attempt(['username' => $user->username, 'password' => ""]);
		if ($loggedIn) {
			$user->last_login_ip = Request::ip();
			$user->save();
			$user->logins()->create([
				'ip' => $user->last_login_ip
			]);
		
			return redirect()->route('room', ['id' => $gameId]);
		} else {
			abort(401, "This user is not supported with API login");
		}
	}

	private function launchValidation($datas) {
		return \Validator::make($datas, [
			'code' => ["required", "string", "max:255"]
		]);
	}
	
	public function launch() {
		$datas = Request::input();
		if ($this->launchValidation($datas)->fails()) {
			return redirect()->route('login');
		}
		// find code
		$aCode = AuthorizationCode::where('code', $datas['code'])->first();
		if (!$aCode || !$aCode->user) {
			return redirect()->route('login');
		}
		// login user
		$loggedIn = Auth::guard('web')->attempt(['username' => $aCode->user->username, 'password' => ""]);
		if (!$loggedIn) {
			return redirect()->route('login');
		}
		// save login IP
		$aCode->user->last_login_ip = Request::ip();
		$aCode->user->save();
		$aCode->user->logins()->create([
			'ip' => $aCode->user->last_login_ip
		]);
		// clear record
		$aCode->delete();
		// redirect
		$roomId = Request::input('room_id');
		if ($roomId) {
			return redirect()->route('room', ['id' => $roomId]);
		} else {
			return redirect()->route('lobby');
		}
	}

	public function getSingleCredit($accountCode) {
		$user = User::where('username', $accountCode)->first();
		if ($user) {
			return response()->json([
				"balance" => $user->balance
			]);
		} else {
			abort(401, "This user is not Exist");
		}
	}
}