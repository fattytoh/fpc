<?php

namespace App\Http\Controllers\API;

use App\Models\AuthorizationCode;
use App\Models\BettingRecord;
use App\Models\User;
use App\Models\Room;
use Validator;
use Auth;
use Exception;
use Http;
use Request;
use Carbon\Carbon;

class APIController extends Controller
{
	private function createValidation($datas) {
		return Validator::make($datas, [
			'users' => ["required", "array", "min:1"],
			'users.*.username' => ["required", "max:255", "string", "unique:users", "distinct"],
			'users.*.name' => ["string", "max:255", "string"],
		]);
	}

	private function updateValidation($datas) {
		return Validator::make($datas, [
			'users' => ["required", "array", "min:1"],
			'users.*.username' => ["required", "string", "max:255", "exists:users,username"],
			'users.*.amount' => ["required", "numeric"],
			'users.*.reason' => ["string"]
		]);
	}
	private function voidValidation($datas) {
		return Validator::make($datas, [
			'users' => ["required", "array", "min:1"],
			'users.*.type' => ["required", "in:Topup,Withdraw"],
			'users.*.username' => ["required", "string", "max:255", "exists:users,username"],
			'users.*.record_id' => ["required", "numeric"],
			'users.*.reason' => ["string"]
		]);
	}

	private function getValidation($datas) {
		return Validator::make($datas, [
			'usernames' => ["required", "array", "min:1"],
			'usernames.*' => ["required", "string", "max:255", "exists:users,username"]
		]);
	}

	private function logValidation($datas) {
		return Validator::make($datas, [
			'usernames' => ["array"],
			'usernames.*' => ["required", "string", "max:255", "exists:users,username"],
			'start' => ["date"],
			'end' => ["date"]
		]);
	}

	private function launchValidation($datas) {
		return \Validator::make($datas, [
			'username' => ["required", "string", "max:255", "exists:users,username"],
			'room_id' => ["integer", "exists:rooms,id"]
		]);
	}
	
	public function token() {
		$auth = Auth::guard("admin");
		if ($auth->attempt(Request::input())) {
			$user = $auth->user();
			$token = \Str::random(60);
			$user->api_token = hash('sha256', $token);
			$user->save();
			return response()->json([
				"token" => $token
			]);
		}
		abort(401);
	}

	public function createUser() {
		$datas = Request::input();
		$this->createValidation($datas)->validate();
		try {
			$users = [];
			foreach($datas['users'] as $userData) {
				$user = User::create([
					'username' => $userData['username'],
					'name' => htmlspecialchars(isset($userData['name']) ? $userData['name'] : $userData['username']),
					'password' => bcrypt(""),
					'invitation_code' => "",
					'real_name' => "",
					'card_number' => "",
					'withdraw_pin' => "",
					'from_wallet' => true,
				]);
				$user->balance = 0;
				$user->won = 0;
				array_push($users, $user->only(['id', 'username', 'name', 'balance', 'won']));
			}
			return response()->json(['users' => $users]);
		} catch (Exception $ex) {
			abort(500, $ex->getMessage());
		}
	}
	
	private function getUser($username, $columns = ['id', 'name', 'username', 'won', 'balance']) {
		return User::where('username', $username)->where('from_wallet', true)->first($columns);
	}
	
	private function getUsersQuery($usernames = [], $with = [], $columns = ['id', 'name', 'username', 'won', 'balance']) {
		$query = User::query();//where('from_wallet', true);
		if (count($usernames) > 0) {
			$query->whereIn('username', $usernames);
		}
		if (count($with) > 0) {
			$query->with($with);
		}
		return $query->select($columns);
	}
	
	private function getUsers($usernames, $with = [], $columns = ['id', 'name', 'username', 'won', 'balance']) {
		return $this->getUsersQuery($usernames, $with, $columns)->get();
	}
	
	public function getCredit() {
		$datas = Request::input();
		$this->getValidation($datas)->validate();
		$users = $this->getUsers($datas['usernames']);
		if ($users->count() > 0) {
			return response()->json(['users' => $users]);
		} else {
			abort(500, $ex->getMessage());
		}
	}
	
	public function adjustPlayerBalance() {
		$datas = Request::input();
		$this->updateValidation($datas)->validate();
		try {
			$admin = parent::getAdmin();
			$users = [];
			foreach ($datas['users'] as $adjustData) {
				$user = $this->getUser($adjustData['username']);
				if ($adjustData['amount'] >= 0) {
					$user->balance += $adjustData['amount'];
					$user->record = $user->topupRecords()->create([
						"amount" => $adjustData['amount'],
						"status" => "充值完成",
						"method_id" => 3,
						"remark" => (isset($adjustData['reason']) ? htmlspecialchars($adjustData['reason']) : null),
						"approved_by" => $admin->id
					])->only(['id', 'amount', 'status', 'remark']);
				} else {
					$user->balance -= $adjustData['amount'];
					$user->record = $user->withdrawRecords()->create([
						"amount" => -$adjustData['amount'],
						"status" => "提取完成",
						"method_id" => 3,
						"remark" => (isset($adjustData['reason']) ? htmlspecialchars($adjustData['reason']) : null),
						"approved_by" => $admin->id
					])->only(['id', 'amount', 'status', 'remark']);
				}
				array_push($users, $user);
			}
			return response()->json(['users' => $users]);
		} catch (Exception $ex) {
			abort(500, $ex->getMessage());
		}
	}

	public function voidAdjust() {
		$datas = Request::input();
		$this->voidValidation($datas)->validate();
		try {
			$admin = parent::getAdmin();
			$users = [];
			foreach ($datas['users'] as $adjustData) {
				$user = $this->getUser($adjustData['username']);
				if ($adjustData['type'] == "topup") {
					$record = $user->topupRecords()->find($adjustData['record_id']);
					if ($record) {
						$user->balance -= $record->amount;
						$user->voided = $record->update([
							"status" => "已退款",
							"remark" => (isset($adjustData['reason']) ? htmlspecialchars($adjustData['reason']) : null),
							"rejected_by" => $admin->id
						]);
					}
				} else {
					$record = $user->withdrawRecords()->find($adjustData['record_id']);
					if ($record) {
						$user->balance -= $record->amount;
						$user->voided = $record->update([
							"status" => "已取消提取",
							"remark" => (isset($adjustData['reason']) ? htmlspecialchars($adjustData['reason']) : null),
							"rejected_by" => $admin->id
						]);
					}
				}
				array_push($users, $user);
			}
			return response()->json(['users' => $users]);
		} catch (Exception $ex) {
			abort(500, $ex->getMessage());
		}
	}
	
	public function gameLogs() {
		$datas = Request::input();
		$this->logValidation($datas)->validate();
		$start = isset($datas['start']) ? Carbon::parse($datas['start']) : null;
		$end = isset($datas['end']) ? Carbon::parse($datas['end']) : null;
		$q = BettingRecord::join("users", "users.id", "betting_records.user_id");
		if (isset($datas['usernames']) && count($datas['usernames']) > 0) {
			$q->whereIn('users.username', $datas['usernames']);
		}
		if ($start)
			$q->where('betting_records.created_at', ">=", $start);
		if ($end)
			$q->where('betting_records.created_at', "<=", $end);
		$q->select(["betting_records.id", "users.username", "betting_records.amount", "betting_records.created_at", "betting_records.won"
			, "betting_records.play_type", "betting_records.play_option", "betting_records.play_group"
			, "betting_records.group", "betting_records.play_name", "betting_records.betting_id"]);
		return response()->json(['records' => $q->get()]);
	}
	
	public function launch() {
		$datas = Request::input();
		$this->launchValidation($datas)->validate();
		$user = $this->getUser($datas['username']);
		if (!$user) {
			abort(404, "Missing or invalid username.");
		}
		$admin = parent::getAdmin();
		$aCode = AuthorizationCode::create([
			'user_id' => $user->id,
			'admin_id' => $admin->id,
			'code' => \Str::random(60)
		]);
		$roomId = Request::input('room_id');
		return response()->json(['redirect_url' => route('launch', ['room_id' => $roomId, 'code' => $aCode->code])]);
	}
	
	public function rooms() {
		return Room::all(['id', 'name', 'icon_url', 'status']);
	}
}