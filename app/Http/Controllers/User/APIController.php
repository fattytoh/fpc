<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Auth;
use Http;
use Request;

class APIController extends Controller
{
	const WALLET_API = "https://test.ahw99.com";	
	const TELEGRAM_WALLET_API = "https://modest-franklin.103-6-245-146.plesk.page";
	
	public function login() {
		$res = Http::post(WALLET_API . "", [
			"cert" => "",
			"agentId" => "",
			"userId" => "",
			"isMobileLogin" => "",
			"externalURL" => "",
			"platform" => "",
			"gameType" => "",
			"gameForbidden" => "",
			"betLimit" => "",
			"autoBetMode" => ""
		]);
	}
	
	public function commit() {
		$user = parent::getUser();
		$withdraw = null;
		
		if ($user->won > 0) {
			$withdraw = $user->withdrawRecords()->create([
				'amount' => $user->won,
				'status' => "",
				'remark' => "结算",
			]);
			
			$res = Http::post(TELEGRAM_WALLET_API . "/endgame", [
				"member_code" => $user->username,
				"member_winpoint" => $user->won
			]);
			
			if ($res->successful()) {
				$resData = $res->json();
				if ($resData['status'] != '0') {
					$withdraw->status = "提取完成";
					$withdraw->save();
					unset($user);
					$user = parent::getUser();
				} else {
					$withdraw->status = "已取消提取";
					$withdraw->remark .= ", 失败原因: " . $resData['msg'];
					$withdraw->save();
					throw new HandledException($resData['msg']);
				}
			} else {
				$withdraw->status = "已取消提取";
				$withdraw->remark .= ", 失败代码: " . $res->status();
				$withdraw->save();
				$res->throw();
			}
		}			
					
		return response()->json([
			"withdraw" => $withdraw,
			"user" => ["total_balance" => number_format($user->totalBalance, 2),
				"won" => $user->won,
				"balance" => $user->balance]
		]);
	}
}