<?php

namespace App\Http\Controllers\User;

use Request;
use Exception;

class TopupController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$user = parent::getUser();
        return view('topup', ['user' => $user]);
    }

    public function topup()
    {
		try {
			$user = parent::getUser();
			$record = $user->topupRecords()->create([
				'amount' => Request::input('amount'),
				'method_id' => Request::input('method_id', null),
				'status' => "等待处理中"
			]);
			return redirect()->back()->with(['message' => "充值请求成功。"]);
		} catch (Exception $ex) {
			return redirect()->back()->withError(['message' => $ex->getMessage()]);
		}
    }
}
