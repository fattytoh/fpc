<?php

namespace App\Http\Controllers\User;

use Exception;
use stdClass;

class InvitationRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('records.invitation');
    }
	
	private function dataset($item) {
		return $item;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$listing = parent::getUser()->inviterRecords()->with(['invited'])->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
