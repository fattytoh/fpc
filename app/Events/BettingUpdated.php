<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BettingUpdated implements ShouldBroadcast//, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $id, $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($room, $last, $current)
    {
        $this->id = $room->id;
		if($current) {
			$current->ended_at_timestamp = $current->ended_at->setTimezone('UTC')->timestamp;
		}
		$this->data = ["last" => $last->only(['id', 'results', 'version'])
			, "current" => $current ? $current->only(['id', 'version', 'ended_at_timestamp']) : null];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\PresenceChannel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('room.' . $this->id);
    }
	
	/**
	 * The event's broadcast name.
	 *
	 * @return string
	 */
	public function broadcastAs()
	{
		return 'betting.updated';
	}
	
	/**	
	 * Get the data to broadcast.
	 *
	 * @return array
	 */
	public function broadcastWith()
	{
		return $this->data;
	}
}
