<?php

namespace App\Console\Commands;

use App\Models\Room;
use App\Events\BettingUpdated;
use Carbon\Carbon;
use Exception;
use Http;
use Illuminate\Console\Command;

class OpenBetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'betting:open {--detail : show detail log} {--force : force open} {--reset : reset for end date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Betting Open';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
	
	private function process($showDetail, $forceOpen, $resetDate) {
		$rooms = Room::all();
		$total = 0;
		$now = Carbon::now();
		echo "开彩发奖中，时间" . $now->toString() . "\n";
		foreach($rooms as $room) {
			if ($room->is_standalone) {
				// skip for standalone
			// } else if ($room->is_api) {
				// $url = "https://www.atm88.org/smilecrm-web/events";
				// $date = $now->clone();
				// if ($now->format("H:i:s") == "00:00:00") {
					// $date = $date->minusDay();
				// }
				// $date = $date->setHour(23)
					// ->setMinute(59)
					// ->setSecond(59);
				// $params = [
					// "per" => 1,
					// "q[start_at_lteq]" => $date->toString(),
					// "q[code_eq]" => "F",
					// "q[s]" => "start_at desc"
				// ];
				// $qs = "";
				// foreach ($params as $key => $value) {
					// if ($qs) {
						// $qs .= "&";
					// } else {
						// $qs .= "?";
					// }
					// $qs .= $key . "=" . urlencode($value);
				// }
				// $res = Http::withHeaders([
					// // 'Authorization' => "bearer brmFG0AST8z8acTZV6aYG_YNRXxuwwUL9QDrz6sq4Sw",
					// 'Accept' => "application/json"
				// ])->get($url . $qs);
				
				// if ($res->successful()) {
					// $attr = $res->json()["data"][0]["attributes"];	
					// if (isset($attr["meta-data"]["results-2d"])) {
						// foreach($room->endedBettings()->where('reward', 0)->get()->reverse() as $betting) {
							// $hour = (int) explode(":", $betting->version)[0];
							// if(isset($attr["meta-data"]["results-2d"][$hour])) {
								// if($betting->total === null || $forceOpen) {
									// if($showDetail) {
										// echo $room->name . "第" . $betting->version . "期彩票已结束，发送奖励中\n";
									// }
									// $betting->results()->create([
										// "order" => 0,
										// "value" => $attr["meta-data"]["results-2d"][$hour]["number"]
									// ]);
									// $betting->deliverReward($resetDate);
									// if($showDetail) {
										// echo "奖励发送完成。\n";
									// }
									// try {
										// event(new BettingUpdated($room, $betting, $room->current));
									// } catch (Exception $ex) {
										// echo "实时更新失效，" . $ex->getMessage() . "。\n";
									// }
									// $total++;
								 // }
							// }
						// }
					// }
				// } else {
					// echo "实时更新失效，API错误代码：" . $res->status() . "。\n";
				// }
			} else if ($room->game_id == 3 || $room->game_id == 4) {
				// ignore 2d and 4d
			} else {
				foreach($room->endedBettings()->where('reward', 0)->get()->reverse() as $betting) {
					if($betting->total === null || $forceOpen) {
						if($showDetail) {
							echo $room->name . "第" . $betting->version . "期彩票已结束，发送奖励中\n";
						}
						if($betting->total === null) {
							$betting->endBetting();
							unset($betting->results);
						}
						$betting->deliverReward($resetDate);
						if($showDetail) {
							echo "奖励发送完成。\n";
						}
						try {
							event(new BettingUpdated($room, $betting, $room->current));
						} catch (Exception $ex) {
							echo "实时更新失效，" . $ex->getMessage() . "。\n";
						}
						$total++;
					}
				}
			}
		}
		echo "已开彩" . $total . "期彩票\n";			
	}

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		try {
			$showDetail = $this->option('detail') ? true : false;
			$forceOpen = $this->option('force') ? true : false;
			$resetDate = $this->option('reset') ? true : false;	
			
			$this->process($showDetail, $forceOpen, $resetDate);
			$now = Carbon::now();
			// sleep until next 15 second
			if (($remain = 15 - $now->second) > 0) {
				sleep($remain);
				$this->process($showDetail, $forceOpen, $resetDate);
			}
			$now = Carbon::now();
			// sleep until next 30 second
			if (($remain = 30 - $now->second) > 0) {
				sleep($remain);
				$this->process($showDetail, $forceOpen, $resetDate);
			}
			$now = Carbon::now();
			// sleep until next 45 second
			if (($remain = 45 - $now->second) > 0) {
				sleep($remain);
				$this->process($showDetail, $forceOpen, $resetDate);
			}
			return 1;
		} catch (Exception $ex) {
			dd($ex);
			echo $ex->getMessage();
			return -1;
		}
        return 0;
    }
}
