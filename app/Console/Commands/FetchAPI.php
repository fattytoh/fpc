<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Betting;
use App\Models\BettingRecord;
use Illuminate\Console\Command;
use Exception;
use Http;
use Carbon\Carbon;
use DB;

class FetchAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'betting:fetch {--detail : show detail log} {--no-open : disable open reward} {--date= : fetch date} {--type= : fetch type} {--game= : game key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$showDetail = $this->option('detail') ? true : false;
		$noOpen = $this->option('no-open') ? true : false;
		$date = ($this->option('date') ? Carbon::parse($this->option('date')) : Carbon::now())->setHour(23)->setMinute(59)->setSecond(59);
		$type = $this->option('type') ? : "";
		$game = $this->option('game') ? : "2d";
		$url = "https://www.atm88.org/smilecrm-web/events";
		$fourDKeys = array_keys(Betting::FOUR_D);
		
		if ($type == "f") {
			// fortune star
			$params = [
				"per" => 1,
				"q[start_at_lteq]" => $date->toString(),
				"q[code_eq]" => "F",
				"q[s]" => "start_at desc"
			];
			$qs = "";
			foreach ($params as $key => $value) {
				if ($qs) {
					$qs .= "&";
				} else {
					$qs .= "?";
				}
				$qs .= $key . "=" . urlencode($value);
			}
		} else if ($type == "" || in_array($type, $fourDKeys)) {
			// non fortune star
			$params = [
				"q[start_at_lteq]" => $date->toString(),
				"q[group_blank]" => "true",
				"q[s]" => "start_at desc"
			];
			$qs = "";
			foreach ($params as $key => $value) {
				if ($qs) {
					$qs .= "&";
				} else {
					$qs .= "?";
				}
				$qs .= $key . "=" . urlencode($value);
			}
		} else {
			if ($showDetail) {
				echo "Not supported type " . $type . ".";
			}
			return 0;
		}
		$res = Http::withHeaders([
			//'Authorization' => "bearer brmFG0AST8z8acTZV6aYG_YNRXxuwwUL9QDrz6sq4Sw",
			'Accept' => "application/json"
		])->get($url . $qs);
		
		if ($res->successful()) {
			$body = $res->json();
			$attrs = array_values(array_filter($body["data"], function($obj) use ($type, $fourDKeys) {
				if (!$obj["attributes"]) {
					return false;
				}
				if ($type == "" && in_array($obj["attributes"]["code"], $fourDKeys)) {
					return true;
				} else if ($type == $obj["attributes"]["code"]) {
					return true;
				}
				return false;
			}));
			if (count($attrs) == 0) {
				if ($showDetail) {
					echo "no result";
				}
				return 0;
			}
			if (!isset($attrs[0]["attributes"]["meta-data"]["v2-results"])) {
				if ($showDetail) {
					echo "no result";
				}
				return 0;
			}
			$game = Game::where('key', $game)->first();
			$groupedData = [];
			//
			$total = 0;
			foreach($attrs as $values) {
				$code = $values["attributes"]["code"];
				if(!isset($groupedData[$code])) {
					$groupedData[$code] = [];
				}
				foreach($values["attributes"]["meta-data"]["v2-results"] as $data) {
					$date = Carbon::parse($values["attributes"]["start-at"])->toDateString();
					$group = $values["attributes"]["meta-data"]["draw-number"];
					if (!isset($groupedData[$code][$group])) {
						$groupedData[$code][$group] = [];
					}
					if(!isset($groupedData[$code][$group][$date])) {
						$groupedData[$code][$group][$date] = [];
					}
					array_push($groupedData[$code][$group][$date], $data);
					$total++;
				}
			}
			$count = 0;
			foreach($groupedData as $key => $groupDatas) {
				foreach($groupDatas as $group => $dateDatas) {
					foreach($dateDatas as $date => $datas) {
						foreach($game->rooms as $room) {
							$betting = $room->bettings()->where('group', $key)->where('version', $group)->first();
							
							if (!$betting || !$betting->ended_at) {
								if (!$noOpen) {
									$betting = $room->upcomingBettings()->where('group', $key)->where('version', "-1")->first();
									if ($betting) {
										$betting->update([
											'date' => $date,
											'version' => $group,
											'ended_at' => Carbon::now()
										]);
										// move 1 counter forward
										DB::statement("UPDATE bettings SET version = version+1 where room_id = {$room->id} and ended_at is null");
									} else {
										$betting = $room->bettings()->create([
											'group' => $key,
											'date' => $date,
											'version' => $group,
											'ended_at' => Carbon::now()
										]);
									}
									$betting->deliverReward();
								} else if (!$betting) {
									$betting = $room->bettings()->create([
										'group' => $key,
										'version' => $group
									]);
								}
							}
							foreach($datas as $index => $data) {
								try {
									$apiRef = $data['key'];
									$result = $betting->results()->where('api_ref', $apiRef)->first();
									if (!$result) {
										$result = $betting->results()->create([
											'order' => $index,
											'value' => substr($data['number'], 0, 2),
											'api_prize' => $data["prize"],
											'api_ref' => $apiRef
										]);
									}
									$count++;
								} catch (Exception $ex) {
									if ($showDetail) {
										echo $ex->getMessage() . ":" . json_encode($data) . "\n\n";
									}
									continue;
								}
							}
						}
					}
				}
			}
			if ($showDetail) {
				echo "Fetched: " . $total . ", inserted: " . $count;
			}
			return 1;
		} else {
			if ($showDetail) {
				echo "Receiving status " . $res->status() . " from api";
			}
			return -1;
		}
    }
}
