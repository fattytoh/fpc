<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRoomRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_rates', function (Blueprint $table) {
            $table->dropColumn('group');
            $table->dropForeign('room_rates_room_id_foreign');
            $table->dropColumn('room_id');
            $table->dropForeign('room_rates_updated_by_foreign');
            $table->dropColumn('updated_by');
			$table->tinyInteger("matched");
        	$table->foreignId('group_id')->nullable()->onDelete('set null')->constrained("room_rate_groups");
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
