<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBettingRecordDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_betting_record_details', function (Blueprint $table) {
            $table->id();
            $table->string('group')->nullable();
            $table->string('group2')->nullable();
            $table->foreignId('record_id')->constraint("betting_records")->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_betting_record_details');
    }
}
