<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBettingRecordRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betting_record_rates', function (Blueprint $table) {
            $table->id();
			$table->tinyInteger('matched');
            $table->decimal('rate', 12, 2);
			$table->foreignId('record_id')->constrained("betting_records")->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('betting_record_rates');
    }
}
