<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterWithdrawRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraw_records', function (Blueprint $table) {
			$table->foreignId('approved_id')->nullable()->constrained("admins")->onDelete('no action');
			$table->foreignId('rejected_id')->nullable()->constrained("admins")->onDelete('no action');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
