<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\BettingRecord;

class MigrateRecordRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		foreach(BettingRecord::all() as $record) {
			if ($record->rates()->count() == 0) {
				$record->rates()->create([
					'matched' => 0,
					'rate' => $record->rate,
				]);
			}
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('migrate_record_rate');
    }
}
