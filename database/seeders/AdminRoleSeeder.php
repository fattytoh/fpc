<?php

namespace Database\Seeders;

use App\Models\AdminRole;
use Illuminate\Database\Seeder;

class AdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [[
			'id' => 1,
			'name' => '程序猿',
			'default_access' => true,
		], [
			'id' => 2,
			'name' => '管理员',
			'default_access' => true,
		], [
			'id' => 3,
			'name' => '分区经理',
			'default_access' => true,
			'regional' => true
		], [
			'id' => 4,
			'name' => '工具人',
			'default_access' => false,
			'regional' => true
		]];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = AdminRole::find($data['id']);
			
			if(!$inst) {
				$inst = AdminRole::create($data);
			}
		}
    }
}
