<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [
			[
				'id' => 1,
				'name' => 'Fish Prawn Crab',
				'key' => 'fpc',
				'min' => 1,
				'max' => 6,
				'count' => 3,
			], [
				'id' => 2,
				'name' => 'Roulette',
				'key' => 'roulette',
				'min' => 0,
				'max' => 99,
				'count' => 1,
			], [
				'id' => 3,
				'name' => '4D',
				'key' => '4d',
				'min' => 0,
				'max' => 9999,
				'count' => 1,
			], [
				'id' => 4,
				'name' => '2D',
				'key' => '2d',
				'min' => 0,
				'max' => 99,
				'count' => 1,
			]
		];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = Game::find($data['id']);
			
			if(!$inst) {
				Game::create($data);
			} else if(config('app.env') !== "production") {
				$inst->update($data);
			}
		}
    }
}
